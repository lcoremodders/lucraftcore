package net.minecraft.client.renderer;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.Nullable;
import javax.vecmath.Matrix4f;

import lucraft.mods.lucraftcore.util.render.IItemRenderer;
import lucraft.mods.lucraftcore.util.render.IItemRendererByEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.client.renderer.entity.RenderItemFrame;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ReportedException;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class LCRenderItem extends RenderItem {

	/* CHANGES:
	 * - moved to net.minecraft.client.renderer for less access transformers
	 * - used reflection instead of access transformes
	 * - removed some comments
	 */
	
    private final RenderItem parent;
    private static LCRenderItem instance;
    private static boolean hasInit;
    
    //Because forge has this private.
    private static final Matrix4f flipX;
    private TextureManager tm;

    //State fields.
    private TransformType lastKnownTransformType;

    static {
        flipX = new Matrix4f();
        flipX.setIdentity();
        flipX.m00 = -1;
    }

    public LCRenderItem(RenderItem renderItem) {
        super(ReflectionHelper.getPrivateValue(RenderItem.class, renderItem, "textureManager", "field_175057_n"), ((ItemModelMesher) ReflectionHelper.getPrivateValue(RenderItem.class, renderItem, "field_175059_m", "itemModelMesher")).getModelManager(), ReflectionHelper.getPrivateValue(RenderItem.class, renderItem, "field_184395_f", "itemColors"));
        this.parent = renderItem;
        this.tm = ReflectionHelper.getPrivateValue(RenderItem.class, renderItem, "textureManager", "field_175057_n");
    }

    public static void init() {
        if (!hasInit) {
            instance = new LCRenderItem(Minecraft.getMinecraft().getRenderItem());
            ReflectionHelper.setPrivateValue(Minecraft.class, Minecraft.getMinecraft(), instance, "field_175621_X", "renderItem");
            hasInit = true;
        }
    }
    
    public static void postInit() {
    	// ItemRenderer
    	ReflectionHelper.setPrivateValue(ItemRenderer.class, Minecraft.getMinecraft().getItemRenderer(), instance, "itemRenderer", "field_178112_h");
    	// RenderEntityItem
    	RenderEntityItem render = (RenderEntityItem) Minecraft.getMinecraft().getRenderManager().entityRenderMap.get(EntityItem.class);
    	ReflectionHelper.setPrivateValue(RenderEntityItem.class, render, instance, "itemRenderer", "field_177080_a");
    	// RenderItemFrame
    	RenderItemFrame render2 = (RenderItemFrame) Minecraft.getMinecraft().getRenderManager().entityRenderMap.get(EntityItemFrame.class);
    	ReflectionHelper.setPrivateValue(RenderItemFrame.class, render2, instance, "itemRenderer", "field_177074_h");
    	
//    	for(Class<? extends Entity> clazz : Minecraft.getMinecraft().getRenderManager().entityRenderMap.keySet()) {
//    		Render<? extends Entity> renders = Minecraft.getMinecraft().getRenderManager().entityRenderMap.get(clazz);
//    		
//    		for(Field f : renders.getClass().getDeclaredFields()) {
//    			try {
//    				f.setAccessible(true);
//					if(f.get(renders) instanceof ItemRenderer) {
//						f.set(renders, instance);
//					}
//				} catch (IllegalArgumentException e) {
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//					e.printStackTrace();
//				}
//    		}
//    	}
    }

    /**
     * Gets the current RenderItem instance, attempts to initialize CCL's if needed.
     *
     * @return The current RenderItem.
     */
    public static RenderItem getOverridenRenderItem() {
        init();
        return Minecraft.getMinecraft().getRenderItem();
    }

    public static void notifyTransform(TransformType transformType) {
        instance.lastKnownTransformType = transformType;
    }

    @Override
    public void renderItem(ItemStack stack, IBakedModel model) {
        if (!stack.isEmpty() && model instanceof IItemRenderer) {
            IItemRenderer renderer = (IItemRenderer) model;
            GlStateManager.pushMatrix();
            GlStateManager.translate(-0.5F, -0.5F, -0.5F);
            
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.enableRescaleNormal();
            GlStateManager.pushMatrix();
            renderer.renderItem(stack, lastKnownTransformType);
            GlStateManager.popMatrix();
            GlStateManager.popMatrix();
            return;
            
        }
        parent.renderItem(stack, model);
    }

    private IBakedModel handleTransforms(ItemStack stack, IBakedModel model, TransformType transformType, boolean isLeftHand) {
        lastKnownTransformType = transformType;
        return ForgeHooksClient.handleCameraTransforms(model, transformType, isLeftHand);
    }

    private boolean isValidModel(IBakedModel model) {
        return model instanceof IItemRenderer;
    }

    @Override
    public void renderItemModel(ItemStack stack, IBakedModel bakedModel, TransformType transform, boolean leftHanded) {
        if (!stack.isEmpty()) {
            if (isValidModel(bakedModel)) {
                tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
                tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).setBlurMipmap(false, false);
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                GlStateManager.enableRescaleNormal();
                GlStateManager.alphaFunc(516, 0.1F);
                GlStateManager.enableBlend();
                GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
                GlStateManager.pushMatrix();
                
                bakedModel = handleTransforms(stack, bakedModel, transform, leftHanded);

                this.renderItem(stack, bakedModel);
                GlStateManager.cullFace(GlStateManager.CullFace.BACK);
                GlStateManager.popMatrix();
                GlStateManager.disableRescaleNormal();
                GlStateManager.disableBlend();
                tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
                tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).restoreLastBlurMipmap();
            } else {
                parent.zLevel = this.zLevel;
                parent.renderItemModel(stack, bakedModel, transform, leftHanded);
            }
        }
    }

    @Override
    public void renderItemModelIntoGUI(ItemStack stack, int x, int y, IBakedModel bakedModel) {
        if (isValidModel(bakedModel)) {
            GlStateManager.pushMatrix();
            tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
            tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).setBlurMipmap(false, false);
            GlStateManager.enableRescaleNormal();
            GlStateManager.enableAlpha();
            GlStateManager.alphaFunc(516, 0.1F);
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            
            try {
				ReflectionHelper.findMethod(RenderItem.class, "setupGuiTransform", "func_180452_a", int.class, int.class, boolean.class).invoke(this, x, y, bakedModel.isGui3d());
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

            bakedModel = handleTransforms(stack, bakedModel, ItemCameraTransforms.TransformType.GUI, false);

            this.renderItem(stack, bakedModel);
            GlStateManager.disableAlpha();
            GlStateManager.disableRescaleNormal();
            GlStateManager.disableLighting();
            GlStateManager.popMatrix();
            tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
            tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).restoreLastBlurMipmap();
        } else {
            parent.zLevel = this.zLevel;
            parent.renderItemModelIntoGUI(stack, x, y, bakedModel);
        }
    }

    // region Other Overrides

    @Override
    public void renderItem(ItemStack stack, TransformType cameraTransformType) {
        if (!stack.isEmpty()) {
            IBakedModel bakedModel = this.getItemModelWithOverrides(stack, null, null);
            if (isValidModel(bakedModel)) {
                this.renderItemModel(stack, bakedModel, cameraTransformType, false);
            }
            parent.zLevel = this.zLevel;
            parent.renderItem(stack, cameraTransformType);
        }
    }

    @Override
    public void renderItem(ItemStack stack, EntityLivingBase livingBase, TransformType transform, boolean leftHanded) {
        if (!stack.isEmpty() && livingBase != null) {
            IBakedModel bakedModel = this.getItemModelWithOverrides(stack, livingBase.world, livingBase);
            if (isValidModel(bakedModel)) {
                if(bakedModel instanceof IItemRendererByEntity) {
                	renderItemIItemRendererByEntity(stack, livingBase, transform, leftHanded, bakedModel);
                } else
                	this.renderItemModel(stack, bakedModel, transform, leftHanded);
            } else {
                parent.zLevel = this.zLevel;
                parent.renderItem(stack, livingBase, transform, leftHanded);
            }
        }
    }
    
    public void renderItemIItemRendererByEntity(ItemStack stack, EntityLivingBase livingBase, TransformType transform, boolean leftHanded, IBakedModel bakedModel) {
        if (!stack.isEmpty()) {
            if (isValidModel(bakedModel)) {
                tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
                tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).setBlurMipmap(false, false);
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                GlStateManager.enableRescaleNormal();
                GlStateManager.alphaFunc(516, 0.1F);
                GlStateManager.enableBlend();
                GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
                GlStateManager.pushMatrix();
                
                bakedModel = handleTransforms(stack, bakedModel, transform, leftHanded);

                IItemRendererByEntity renderer = (IItemRendererByEntity) bakedModel;
                GlStateManager.pushMatrix();
                GlStateManager.translate(-0.5F, -0.5F, -0.5F);
                
                GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
                GlStateManager.enableRescaleNormal();
                GlStateManager.pushMatrix();
                renderer.renderItem(stack, transform, livingBase, leftHanded);
                GlStateManager.popMatrix();
                GlStateManager.popMatrix();
                
                GlStateManager.cullFace(GlStateManager.CullFace.BACK);
                GlStateManager.popMatrix();
                GlStateManager.disableRescaleNormal();
                GlStateManager.disableBlend();
                tm.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
                tm.getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).restoreLastBlurMipmap();
            } else {
                parent.zLevel = this.zLevel;
                parent.renderItemModel(stack, bakedModel, transform, leftHanded);
            }
        }
    }

    @Override
    public void renderItemIntoGUI(ItemStack stack, int x, int y) {
        IBakedModel bakedModel = this.getItemModelWithOverrides(stack, null, null);
        if (isValidModel(bakedModel)) {
            this.renderItemModelIntoGUI(stack, x, y, bakedModel);
        } else {
            parent.zLevel = this.zLevel;
            parent.renderItemIntoGUI(stack, x, y);
        }
    }

    @Override
    public void renderItemAndEffectIntoGUI(ItemStack stack, int xPosition, int yPosition) {
        this.renderItemAndEffectIntoGUI(Minecraft.getMinecraft().player, stack, xPosition, yPosition);
    }

    @Override
    public void renderItemAndEffectIntoGUI(@Nullable EntityLivingBase livingBase, final ItemStack stack, int x, int y) {
        if (!stack.isEmpty()) {
            try {
                IBakedModel model = this.getItemModelWithOverrides(stack, null, livingBase);
                if (isValidModel(model)) {
                    this.zLevel += 50.0F;
                    this.renderItemModelIntoGUI(stack, x, y, model);
                    this.zLevel -= 50.0F;
                } else {
                    parent.zLevel = this.zLevel;
                    parent.renderItemAndEffectIntoGUI(livingBase, stack, x, y);
                }

            } catch (Throwable throwable) {
                CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Rendering item");
                CrashReportCategory crashreportcategory = crashreport.makeCategory("Item being rendered");
                crashreportcategory.addDetail("Item Type", () -> String.valueOf(stack.getItem()));
                crashreportcategory.addDetail("Item Aux", () -> String.valueOf(stack.getMetadata()));
                crashreportcategory.addDetail("Item NBT", () -> String.valueOf(stack.getTagCompound()));
                crashreportcategory.addDetail("Item Foil", () -> String.valueOf(stack.hasEffect()));
                throw new ReportedException(crashreport);
            }
        }
    }

//    @Override
//    public void registerItem(Item item, int subType, String identifier) {
//        parent.registerItem(item, subType, identifier);
//    }

    @Override
    public ItemModelMesher getItemModelMesher() {
        return parent.getItemModelMesher();
    }

    @Override
    public boolean shouldRenderItemIn3D(ItemStack stack) {
        return parent.shouldRenderItemIn3D(stack);
    }

    @Override
    public IBakedModel getItemModelWithOverrides(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entitylivingbaseIn) {
        return parent.getItemModelWithOverrides(stack, worldIn, entitylivingbaseIn);
    }

    @Override
    public void onResourceManagerReload(IResourceManager resourceManager) {
        parent.onResourceManagerReload(resourceManager);
    }
}