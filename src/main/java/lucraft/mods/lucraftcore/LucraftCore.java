package lucraft.mods.lucraftcore;

import lucraft.mods.lucraftcore.LCConfig.MessageSendModuleSettings;
import lucraft.mods.lucraftcore.LCConfig.MessageSyncConfig;
import lucraft.mods.lucraftcore.addonpacks.ModuleAddonPacks;
import lucraft.mods.lucraftcore.extendedinventory.ModuleExtendedInventory;
import lucraft.mods.lucraftcore.karma.ModuleKarma;
import lucraft.mods.lucraftcore.materials.ModuleMaterials;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.proxies.LCCommonProxy;
import lucraft.mods.lucraftcore.sizechanging.ModuleSizeChanging;
import lucraft.mods.lucraftcore.superpowers.ModuleSuperpowers;
import lucraft.mods.lucraftcore.util.events.PlayerEmptyClickEvent.MessagePlayerEmptyClickEvent;
import lucraft.mods.lucraftcore.util.gui.LCGuiHandler;
import lucraft.mods.lucraftcore.util.items.OpenableArmor.MessageToggleArmor;
import lucraft.mods.lucraftcore.util.network.MessageSyncPotionEffects;
import lucraft.mods.lucraftcore.util.triggers.LCCriteriaTriggers;
import lucraft.mods.lucraftcore.utilities.ModuleUtilities;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.registry.RegistryNamespaced;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Consumer;

@Mod(modid = LucraftCore.MODID, version = LucraftCore.VERSION, name = LucraftCore.NAME, dependencies = LucraftCore.DEPENDENCIES)
public class LucraftCore {
	
	public static final String NAME = "Lucraft: Core";
	public static final String MODID = "lucraftcore";
	public static final String VERSION = "1.12.2-2.0.5";
	public static final String DEPENDENCIES = "";
	
	@SidedProxy(clientSide = "lucraft.mods.lucraftcore.proxies.LCClientProxy", serverSide = "lucraft.mods.lucraftcore.proxies.LCCommonProxy")
	public static LCCommonProxy proxy;
	
	@Instance(value = LucraftCore.MODID)
	public static LucraftCore INSTANCE;

	public static ItemStack CREATIVE_TAB_ICON = new ItemStack(Blocks.BARRIER);
	
	public static CreativeTabs CREATIVE_TAB = new CreativeTabs("tabLucraftCore") {
		
		@Override
		public ItemStack getTabIconItem() {
			return CREATIVE_TAB_ICON;
		}

	};
	
	public static final RegistryNamespaced<String, Module> MODULES = new RegistryNamespaced<>();
	
	public static Logger LOGGER = LogManager.getLogger(MODID);

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Packets
		LCPacketDispatcher.registerMessage(MessageSyncConfig.Handler.class, MessageSyncConfig.class, Side.CLIENT, 0);
		LCPacketDispatcher.registerMessage(MessageSendModuleSettings.Handler.class, MessageSendModuleSettings.class, Side.SERVER, 1);
		LCPacketDispatcher.registerMessage(MessagePlayerEmptyClickEvent.Handler.class, MessagePlayerEmptyClickEvent.class, Side.SERVER, 2);
		LCPacketDispatcher.registerMessage(MessageSyncPotionEffects.Handler.class, MessageSyncPotionEffects.class, Side.CLIENT, 3);
		LCPacketDispatcher.registerMessage(MessageToggleArmor.Handler.class, MessageToggleArmor.class, Side.SERVER, 4);
		
		// Gui Handler
		NetworkRegistry.INSTANCE.registerGuiHandler(LucraftCore.INSTANCE, new LCGuiHandler());
		
		registerModule(ModuleUtilities.INSTANCE);
		registerModule(ModuleAddonPacks.INSTANCE);
		if(LCConfig.modules.materials)
			registerModule(ModuleMaterials.INSTANCE);
		if(LCConfig.modules.superpowers)
			registerModule(ModuleSuperpowers.INSTANCE);
		if(LCConfig.modules.extended_inventory)
			registerModule(ModuleExtendedInventory.INSTANCE);
		if(LCConfig.modules.karma)
			registerModule(ModuleKarma.INSTANCE);
		if(LCConfig.modules.size_changing)
			registerModule(ModuleSizeChanging.INSTANCE);
		
		MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				LOGGER.info("Module '" + t.getName() + "' started pre-initialization");
				t.preInit(event);
			}
		});
		
		proxy.preInit(event);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				LOGGER.info("Module '" + t.getName() + "' started initialization");
				t.init(event);
			}
		});
		
		proxy.init(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				LOGGER.info("Module '" + t.getName() + "' started post-initialization");
				t.postInit(event);
			}
		});
		
		proxy.postInit(event);
		
		// Criteria Triggers
		LCCriteriaTriggers.init();
	}
	
	@EventHandler
	public void init(FMLServerStartingEvent e) {
		MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				t.onServerStarting(e);
			}
		});
	}
	
	private static int moduleId = 0;
	
	public static void registerModule(Module module) {
		MODULES.register(moduleId, module.getName(), module);
		LOGGER.info("Registered module '" + module.getName() + "'");
		moduleId++;
	}
	
}