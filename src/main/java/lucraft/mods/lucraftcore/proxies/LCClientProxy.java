package lucraft.mods.lucraftcore.proxies;

import java.util.function.Consumer;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.extendedinventory.gui.InventoryTabExtendedInventory;
import lucraft.mods.lucraftcore.karma.gui.InventoryTabKarma;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.superpowers.gui.InventoryTabSuitSetAbilities;
import lucraft.mods.lucraftcore.superpowers.gui.InventoryTabSuperpowerAbilities;
import lucraft.mods.lucraftcore.util.items.OpenableArmor;
import lucraft.mods.lucraftcore.util.updatechecker.UpdateChecker;
import micdoodle8.mods.galacticraft.api.client.tabs.InventoryTabVanilla;
import micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LCRenderItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class LCClientProxy extends LCCommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		
		// Openable Armor
		OpenableArmor.init();
		
		LucraftCore.MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				t.preInitClient(event);
			}
		});
	}

	@Override
	public void init(FMLInitializationEvent event) {
		super.init(event);
		
		// RenderItem,
		LCRenderItem.init();
		
		// UpdateChecker
		if(LCConfig.updateChecker)
			new UpdateChecker(LucraftCore.VERSION, TextFormatting.DARK_GRAY + "[" + TextFormatting.WHITE + "Lucraft: Core" + TextFormatting.DARK_GRAY + "]", "http://mods.curse.com/mc-mods/minecraft/230651-lucraft-core", "https://drive.google.com/uc?export=download&id=0B6_wwPkl6fmOVFhpcmxrVmxCbWM");
		
		LucraftCore.MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				t.initClient(event);
			}
		});
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);

		LucraftCore.MODULES.forEach(new Consumer<Module>() {
			@Override
			public void accept(Module t) {
				t.postInitClient(event);
			}
		});

		// Inventory Tabs
		MinecraftForge.EVENT_BUS.register(new TabRegistry());
		
		if (TabRegistry.getTabList().size() < 2)
			TabRegistry.registerTab(new InventoryTabVanilla());
		
		TabRegistry.registerTab(new InventoryTabSuperpowerAbilities());
		TabRegistry.registerTab(new InventoryTabSuitSetAbilities());
		
		if (LCConfig.modules.extended_inventory) TabRegistry.registerTab(new InventoryTabExtendedInventory());
		if (LCConfig.modules.karma) TabRegistry.registerTab(new InventoryTabKarma());
		
		LCRenderItem.postInit();
	}

	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return (ctx.side.isClient() ? Minecraft.getMinecraft().player : super.getPlayerEntity(ctx));
	}

	@Override
	public IThreadListener getThreadFromContext(MessageContext ctx) {
		return (ctx.side.isClient() ? Minecraft.getMinecraft() : super.getThreadFromContext(ctx));
	}

}
