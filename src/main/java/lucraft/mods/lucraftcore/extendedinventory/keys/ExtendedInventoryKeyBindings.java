package lucraft.mods.lucraftcore.extendedinventory.keys;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.extendedinventory.IItemExtendedInventory.ExtendedInventoryItemType;
import lucraft.mods.lucraftcore.extendedinventory.events.ExtendedInventoryKeyEvent;
import lucraft.mods.lucraftcore.extendedinventory.network.MessageExtendedInventoryKey;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.client.settings.IKeyConflictContext;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

public class ExtendedInventoryKeyBindings {

	public static ArrayList<KeyBindingExtendedInventory> KEYS = new ArrayList<KeyBindingExtendedInventory>();
	
	public static KeyBindingExtendedInventory NECKLACE = new KeyBindingExtendedInventory("lucraftcore.keybinding.keyNecklace", KeyConflictContext.IN_GAME, Keyboard.KEY_NUMPAD1, LucraftCore.NAME, ExtendedInventoryItemType.NECKLACE);
	public static KeyBindingExtendedInventory MANTLE = new KeyBindingExtendedInventory("lucraftcore.keybinding.keyMantle", KeyConflictContext.IN_GAME, Keyboard.KEY_NUMPAD2, LucraftCore.NAME, ExtendedInventoryItemType.MANTLE);
	public static KeyBindingExtendedInventory WRIST = new KeyBindingExtendedInventory("lucraftcore.keybinding.keyWrist", KeyConflictContext.IN_GAME, Keyboard.KEY_NUMPAD3, LucraftCore.NAME, ExtendedInventoryItemType.WRIST);
	
	public ExtendedInventoryKeyBindings() {
		registerKeyBinding(NECKLACE);
		registerKeyBinding(MANTLE);
		registerKeyBinding(WRIST);
	}
	
	public static void registerKeyBinding(KeyBindingExtendedInventory key) {
		ClientRegistry.registerKeyBinding(key);
		KEYS.add(key);
	}
	
	@SubscribeEvent
	public void onKey(KeyInputEvent evt) {
		for (KeyBindingExtendedInventory key : KEYS) {
			if (key.isKeyDown() != key.isPressed) {
				key.isPressed = !key.isPressed;
				MinecraftForge.EVENT_BUS.post(new ExtendedInventoryKeyEvent.Client(key.getItemType(), key.isPressed));
			}
		}
	}

	@SubscribeEvent
	public void onLucraftKey(ExtendedInventoryKeyEvent.Client e) {
		LCPacketDispatcher.sendToServer(new MessageExtendedInventoryKey(e.pressed, e.type));
	}
	
	public static class KeyBindingExtendedInventory extends KeyBinding {

		protected ExtendedInventoryItemType type;
		public boolean isPressed;
		
		public KeyBindingExtendedInventory(String description, IKeyConflictContext keyConflictContext, int keyCode, String category, ExtendedInventoryItemType type) {
			super(description, keyConflictContext, keyCode, category);
			this.type = type;
		}
		
		public ExtendedInventoryItemType getItemType() {
			return type;
		}
		
	}
	
}
