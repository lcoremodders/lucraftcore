package lucraft.mods.lucraftcore.util.items;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.EntityEquipmentSlot.Type;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

@EventBusSubscriber(modid = LucraftCore.MODID, value = Side.CLIENT)
public class OpenableArmor {

	public static KeyBinding TOGGLE_ARMOR = new KeyBinding("lucraftcore.keybinding.keyOpenSuit", KeyConflictContext.IN_GAME, Keyboard.KEY_C, LucraftCore.NAME);

	public static void init() {
		ClientRegistry.registerKeyBinding(TOGGLE_ARMOR);
	}

	@SubscribeEvent
	public static void onKey(KeyInputEvent e) {
		if (TOGGLE_ARMOR.isKeyDown())
			LCPacketDispatcher.sendToServer(new MessageToggleArmor());
	}

	public static interface IOpenableArmor {

		public abstract void setArmorOpen(Entity entity, ItemStack stack, boolean open);

		public abstract boolean isArmorOpen(Entity entity, ItemStack stack);

		public void onArmorToggled(Entity entity, ItemStack stack, boolean open);

	}
	
	public static class MessageToggleArmor implements IMessage {

		public MessageToggleArmor() {
		}
		
		@Override
		public void fromBytes(ByteBuf buf) {
			
		}

		@Override
		public void toBytes(ByteBuf buf) {
			
		}
		
		public static class Handler extends AbstractServerMessageHandler<MessageToggleArmor> {

			@Override
			public IMessage handleServerMessage(EntityPlayer player, MessageToggleArmor message, MessageContext ctx) {
				
				LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

					@Override
					public void run() {
						List<ItemStack> list = new ArrayList<>();
						
						for(EntityEquipmentSlot slots : EntityEquipmentSlot.values()) {
							if(slots.getSlotType() == Type.ARMOR && !player.getItemStackFromSlot(slots).isEmpty() && player.getItemStackFromSlot(slots).getItem() instanceof IOpenableArmor) {
								list.add(player.getItemStackFromSlot(slots));
							}
						}
						
						boolean isAllClosed = true;
						for(ItemStack stack : list) {
							IOpenableArmor armor = (IOpenableArmor)stack.getItem();
							if(armor.isArmorOpen(player, stack))
								isAllClosed = false;
						}
						
						for(ItemStack stack : list) {
							IOpenableArmor armor = (IOpenableArmor)stack.getItem();
							if(armor.isArmorOpen(player, stack) != isAllClosed) {
								armor.setArmorOpen(player, stack, isAllClosed);
								armor.onArmorToggled(player, stack, isAllClosed);
							}
						}
					}
					
				});
				
				return null;
			}
			
		}
		
	}
}
