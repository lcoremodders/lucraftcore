package lucraft.mods.lucraftcore.util.sounds;

import lucraft.mods.lucraftcore.LucraftCore;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber(modid = LucraftCore.MODID)
public class LCSoundEvents {

	public static SoundEvent ENERGY_BLAST;
	
	@SubscribeEvent
	public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> e) {
		e.getRegistry().register(ENERGY_BLAST = new SoundEvent(new ResourceLocation(LucraftCore.MODID, "energy_blast")).setRegistryName(new ResourceLocation(LucraftCore.MODID, "energy_blast")));
	}
	
}
