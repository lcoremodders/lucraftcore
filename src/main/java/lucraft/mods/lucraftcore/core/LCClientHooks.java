package lucraft.mods.lucraftcore.core;

import lucraft.mods.lucraftcore.addonpacks.AddonPackHandler;
import lucraft.mods.lucraftcore.addonpacks.ModuleAddonPacks;
import lucraft.mods.lucraftcore.addonpacks.resourcepacks.AddonPackFolderResourcePack;
import lucraft.mods.lucraftcore.addonpacks.resourcepacks.AddonPackResourcePack;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;
import java.util.List;

public class LCClientHooks {

	public static void preRenderCallBack(RenderLivingBase<EntityLivingBase> renderer, EntityLivingBase entity) {
		RenderModelEvent ev = new RenderModelEvent(entity, renderer);
		MinecraftForge.EVENT_BUS.post(ev);
	}
	
	public static void renderBipedPre(ModelBiped model, Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		RenderModelEvent.SetRotationAngels ev = new RenderModelEvent.SetRotationAngels(entity, model, f, f1, f2, f3, f4, f5, RenderModelEvent.ModelSetRotationAnglesEventType.PRE);
		MinecraftForge.EVENT_BUS.post(ev);
		
		if(!ev.isCanceled()) {
			model.setRotationAngles(ev.limbSwing, ev.limbSwingAmount, ev.partialTicks, ev.ageInTicks, ev.netHeadYaw, ev.headPitch, ev.getEntity());
		}
	}

	public static void renderBipedPost(ModelBiped model, Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		RenderModelEvent.SetRotationAngels ev = new RenderModelEvent.SetRotationAngels(entity, model, f, f1, f2, f3, f4, f5, RenderModelEvent.ModelSetRotationAnglesEventType.POST);
		MinecraftForge.EVENT_BUS.post(ev);
	}
	
	public static void insertAddonPackResourcePacks(List resourcePacks) {
		if(!ModuleAddonPacks.INSTANCE.isEnabled())
			return;
		
		if (!AddonPackHandler.ADDON_PACKS_DIR.exists()) {
			AddonPackHandler.ADDON_PACKS_DIR.mkdir();
		}

		for(File file : AddonPackHandler.ADDON_PACKS_DIR.listFiles()) {
			if(file.isDirectory()) {
				resourcePacks.add(new AddonPackFolderResourcePack(file));
			} else {
				resourcePacks.add(new AddonPackResourcePack(file));
			}
		}
	}
	
}
