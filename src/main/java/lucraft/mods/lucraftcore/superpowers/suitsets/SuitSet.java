package lucraft.mods.lucraftcore.superpowers.suitsets;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.effects.Effect;
import lucraft.mods.lucraftcore.superpowers.effects.EffectVibrating;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.superpowers.models.ModelBipedSuitSet;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketCustomSound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public abstract class SuitSet {

	private String name;

	public SuitSet(String name) {
		this.name = name;
	}

	public String getUnlocalizedName() {
		return name;
	}

	public String getRegistryName() {
		return StringHelper.unlocalizedToResourceName(getUnlocalizedName());
	}

	public boolean canOpenArmor(EntityEquipmentSlot slot) {
		return false;
	}

	public void onArmorToggled(Entity entity, ItemStack stack, EntityEquipmentSlot slot, boolean open) {
		if (entity instanceof EntityPlayerMP)
			((EntityPlayerMP) entity).connection.sendPacket(new SPacketCustomSound(SoundEvents.ITEM_ARMOR_EQUIP_IRON.getRegistryName().toString(), entity.getSoundCategory(), entity.posX, entity.posY, entity.posZ, 1F, 1F));
	}

	public String getDisplayName() {
		return StringHelper.translateToLocal(getModId().toLowerCase() + ".suit." + getUnlocalizedName() + ".name");
	}

	public abstract String getModId();

	public String getDisplayNameForItem(Item item, ItemStack stack, EntityEquipmentSlot armorType, String origName) {
		return origName;
	}

	public String getArmorTexturePath(ItemStack stack, Entity entity, EntityEquipmentSlot slot, boolean light, boolean smallArms, boolean open) {
		String tex = slot == EntityEquipmentSlot.HEAD ? "helmet" : slot == EntityEquipmentSlot.CHEST ? "chestplate" : slot == EntityEquipmentSlot.LEGS ? "legs" : "boots";

		if (slot == EntityEquipmentSlot.CHEST && smallArms)
			tex = tex + "_smallarms";
		if (this.canOpenArmor(slot) && open)
			tex = tex + "_open";
		if (light)
			tex = tex + "_lights";

		return getModId() + ":textures/models/armor/" + this.getRegistryName() + "/" + tex + ".png";
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(ItemStack stack, Entity entity, EntityEquipmentSlot slot, boolean light, boolean smallArms, boolean open) {
		return new ModelBipedSuitSet(getArmorModelScale(slot), getArmorTexturePath(stack, entity, slot, false, smallArms, open), getArmorTexturePath(stack, entity, slot, true, smallArms, open), this, slot, smallArms, EffectVibrating.isVibrating(entity));
	}

	public abstract ArmorMaterial getArmorMaterial(EntityEquipmentSlot slot);

	public boolean hasGlowyThings(EntityLivingBase entity, EntityEquipmentSlot slot) {
		return false;
	}

	public boolean showInCreativeTab() {
		return getCreativeTab() != null;
	}

	public CreativeTabs getCreativeTab() {
		return CreativeTabs.COMBAT;
	}

	public boolean hasArmorOn(EntityLivingBase entity) {
		boolean hasArmorOn = true;

		if (getHelmet() != null && (entity.getItemStackFromSlot(EntityEquipmentSlot.HEAD).isEmpty() || entity.getItemStackFromSlot(EntityEquipmentSlot.HEAD).getItem() != getHelmet()))
			hasArmorOn = false;

		if (getLegs() != null && (entity.getItemStackFromSlot(EntityEquipmentSlot.LEGS).isEmpty() || entity.getItemStackFromSlot(EntityEquipmentSlot.LEGS).getItem() != getLegs()))
			hasArmorOn = false;

		if (getBoots() != null && (entity.getItemStackFromSlot(EntityEquipmentSlot.FEET).isEmpty() || entity.getItemStackFromSlot(EntityEquipmentSlot.FEET).getItem() != getBoots()))
			hasArmorOn = false;

		return hasArmorOn;
	}

	public Item getHelmet() {
		return null;
	}

	public Item getChestplate() {
		return null;
	}

	public Item getLegs() {
		return null;
	}

	public Item getBoots() {
		return null;
	}

	public ItemStack getRepresentativeItem() {
		return new ItemStack(this.getChestplate());
	}
	
	public List<Effect> getEffects() {
		return null;
	}
	
	public NBTTagCompound getData() {
		return null;
	}

	public float getArmorModelScale(EntityEquipmentSlot armorSlot) {
		if (armorSlot == EntityEquipmentSlot.HEAD)
			return 0.5F;
		else if (armorSlot == EntityEquipmentSlot.CHEST || armorSlot == EntityEquipmentSlot.FEET)
			return 0.252F;
		return 0.25F;
	}

	public boolean hasExtraDescription(ItemStack stack) {
		return getExtraDescription(stack) != null && getExtraDescription(stack).size() > 0;
	}

	public List<String> getExtraDescription(ItemStack stack) {
		return null;
	}

	protected List<Ability> addDefaultAbilities(EntityPlayer player, List<Ability> list) {
		return list;
	}

	public final List<Ability> getDefaultAbilities(EntityPlayer player, List<Ability> list) {
		list = addDefaultAbilities(player, list);
		for(Ability ab : list)
			ab.context = Ability.EnumAbilityContext.SUIT;
		List<Ability> l = Ability.removeDisabledAbilities(list);
		for(Ability ab : l)
			ab.init(l);
		return l;
	}

	public Ability getSuitAbilityForKey(AbilityKeys key, List<Ability> list) {
		return null;
	}

	public void onEquip(EntityPlayer player) {

	}

	public void onUnequip(EntityPlayer player) {

	}

	public void onUpdate(EntityPlayer player) {

	}

	public static SuitSet getSuitSet(EntityLivingBase entity) {
		if (entity instanceof EntityPlayer)
			return hasSuitSetOn(entity) ? getSuitSet(entity.getItemStackFromSlot(EntityEquipmentSlot.CHEST)) : null;
		return null;
	}

	public static SuitSet getSuitSet(ItemStack stack) {
		if (stack.getItem() != null && stack.getItem() instanceof ItemSuitSetArmor) {
			return ((ItemSuitSetArmor) stack.getItem()).getSuitSet();
		}

		return null;
	}

	public static boolean hasSuitSetOn(EntityLivingBase entity) {
		if (!entity.getItemStackFromSlot(EntityEquipmentSlot.CHEST).isEmpty() && entity.getItemStackFromSlot(EntityEquipmentSlot.CHEST).getItem() instanceof ItemSuitSetArmor) {
			return ((ItemSuitSetArmor) entity.getItemStackFromSlot(EntityEquipmentSlot.CHEST).getItem()).getSuitSet().hasArmorOn(entity);
		}

		return false;
	}

}