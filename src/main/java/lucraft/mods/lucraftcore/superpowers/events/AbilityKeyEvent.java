package lucraft.mods.lucraftcore.superpowers.events;

import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.relauncher.Side;

public class AbilityKeyEvent extends Event {

	public Side side;
	public AbilityKeys type;
	public boolean pressed;

	private AbilityKeyEvent(AbilityKeys type, Side side, boolean pressed) {
		this.type = type;
		this.side = side;
		this.pressed = pressed;
	}

	public static class Client extends AbilityKeyEvent {

		public Client(AbilityKeys type, boolean pressed) {
			super(type, Side.CLIENT, pressed);
		}

	}

	@Cancelable
	public static class Server extends AbilityKeyEvent {

		public EntityPlayer player;

		public Server(AbilityKeys type, EntityPlayer player, boolean pressed) {
			super(type, Side.SERVER, pressed);
			this.player = player;
		}

	}

}
