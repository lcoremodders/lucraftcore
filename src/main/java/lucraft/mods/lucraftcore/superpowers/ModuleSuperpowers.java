package lucraft.mods.lucraftcore.superpowers;

import java.util.Map;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilitybar.AbilityBarMainHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.commands.CommandSuperpower;
import lucraft.mods.lucraftcore.superpowers.commands.CommandSuperpowerXP;
import lucraft.mods.lucraftcore.superpowers.effects.EffectTrail.EntityTrail;
import lucraft.mods.lucraftcore.superpowers.entities.EntityEnergyBlast;
import lucraft.mods.lucraftcore.superpowers.items.SuperpowerItems;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeyBindings;
import lucraft.mods.lucraftcore.superpowers.network.MessageAbilityKey;
import lucraft.mods.lucraftcore.superpowers.network.MessageChooseSuperpower;
import lucraft.mods.lucraftcore.superpowers.network.MessageChooseSuperpowerGui;
import lucraft.mods.lucraftcore.superpowers.network.MessageSuperpowerStyle;
import lucraft.mods.lucraftcore.superpowers.network.MessageSyncJsonSuitSet;
import lucraft.mods.lucraftcore.superpowers.network.MessageSyncJsonSuperpower;
import lucraft.mods.lucraftcore.superpowers.network.MessageSyncSuperpower;
import lucraft.mods.lucraftcore.superpowers.network.MessageToggleAbilityVisibility;
import lucraft.mods.lucraftcore.superpowers.render.RenderEntityEnergyBlast;
import lucraft.mods.lucraftcore.superpowers.render.RenderEntityTrail;
import lucraft.mods.lucraftcore.superpowers.render.SuperpowerRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModuleSuperpowers extends Module {

	public static final ModuleSuperpowers INSTANCE = new ModuleSuperpowers();
	
	public SuperpowerItems ITEMS = new SuperpowerItems();
	
	@Override
	public void preInit(FMLPreInitializationEvent event) {
		// Capability Registering
		CapabilityManager.INSTANCE.register(ISuperpowerCapability.class, new CapabilitySuperpower.CapabilitySuperpowerStorage(), CapabilitySuperpower.class);
		
		// EventHandler Registering
		MinecraftForge.EVENT_BUS.register(ITEMS);
		MinecraftForge.EVENT_BUS.register(new CapabilitySuperpower.CapabilitySuperpowerEventHandler());
	}

	@Override
	public void init(FMLInitializationEvent event) {
		// Packet Registering
		LCPacketDispatcher.registerMessage(MessageSyncSuperpower.Handler.class, MessageSyncSuperpower.class, Side.CLIENT, 40);
		LCPacketDispatcher.registerMessage(MessageAbilityKey.Handler.class, MessageAbilityKey.class, Side.SERVER, 41);
		LCPacketDispatcher.registerMessage(MessageToggleAbilityVisibility.Handler.class, MessageToggleAbilityVisibility.class, Side.CLIENT, 42);
		LCPacketDispatcher.registerMessage(MessageSuperpowerStyle.Handler.class, MessageSuperpowerStyle.class, Side.SERVER, 43);
		LCPacketDispatcher.registerMessage(MessageChooseSuperpowerGui.Handler.class, MessageChooseSuperpowerGui.class, Side.CLIENT, 44);
		LCPacketDispatcher.registerMessage(MessageChooseSuperpower.Handler.class, MessageChooseSuperpower.class, Side.SERVER, 45);
		LCPacketDispatcher.registerMessage(MessageSyncJsonSuperpower.Handler.class, MessageSyncJsonSuperpower.class, Side.CLIENT, 46);
		LCPacketDispatcher.registerMessage(MessageSyncJsonSuitSet.Handler.class, MessageSyncJsonSuitSet.class, Side.CLIENT, 47);
		
		// Injections
		ITEMS.loadSuperpowerInjection();
		
		// Entities
		EntityRegistry.registerModEntity(new ResourceLocation(LucraftCore.MODID, "energy_blast"), EntityEnergyBlast.class, "energy_blast", 0, LucraftCore.INSTANCE, 100, 1, true);
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void preInitClient(FMLPreInitializationEvent event) {
		// Key Bindings
		MinecraftForge.EVENT_BUS.register(new AbilityKeyBindings());
	}
	
	@SuppressWarnings("deprecation")
	@SideOnly(Side.CLIENT)
	@Override
	public void initClient(FMLInitializationEvent event) {
		Map<String, RenderPlayer> skinMap = Minecraft.getMinecraft().getRenderManager().getSkinMap();
		RenderPlayer render;
		render = skinMap.get("default");
		render.addLayer(new SuperpowerRenderer(render));

		render = skinMap.get("slim");
		render.addLayer(new SuperpowerRenderer(render));
		
		MinecraftForge.EVENT_BUS.register(new AbilityBarMainHandler());
		
		RenderingRegistry.registerEntityRenderingHandler(EntityTrail.class, new RenderEntityTrail(Minecraft.getMinecraft().getRenderManager()));
		RenderingRegistry.registerEntityRenderingHandler(EntityEnergyBlast.class, new RenderEntityEnergyBlast(Minecraft.getMinecraft().getRenderManager()));
	}
	
	@Override
	public void onServerStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandSuperpower());
		event.registerServerCommand(new CommandSuperpowerXP());
	}
	
	@Override
	public String getName() {
		return "Superpowers";
	}
	
	@Override
	public boolean isEnabled() {
		return LCConfig.modules.superpowers;
	}

}
