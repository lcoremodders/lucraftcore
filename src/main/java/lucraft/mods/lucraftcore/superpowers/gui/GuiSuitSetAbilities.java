package lucraft.mods.lucraftcore.superpowers.gui;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.container.ContainerDummy;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButton10x;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GuiSuitSetAbilities extends GuiContainer {

	public static final ResourceLocation TEX = new ResourceLocation(LucraftCore.MODID, "textures/gui/abilities.png");

	public EntityPlayer player;
	public GuiSuitAbilityList list;
	public SuitSetAbilityHandler handler;

	public int xSize_ = 256;
	public int ySize_ = 189;

	public int selectedAbility = -1;

	public GuiSuitSetAbilities(EntityPlayer player) {
		super(new ContainerDummy());
		this.player = player;
		this.handler = SuperpowerHandler.getSuitSetAbilityHandler(player);
	}

	@Override
	public void initGui() {
		super.initGui();
		this.xSize = 256;
		this.ySize = 189;
		this.xSize_ = xSize;
		this.ySize_ = ySize;

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		this.buttonList.add(new GuiButton10x(30, i + 239, j + 90, "?"));
		list = new GuiSuitAbilityList(mc, this);
		
		int cornerX = i;
		int cornerY = j;

		TabRegistry.updateTabValues(cornerX, cornerY, InventoryTabSuitSetAbilities.class);
		TabRegistry.addTabsToList(this.buttonList);
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1, 1, 1);

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		mc.getTextureManager().bindTexture(TEX);
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

		{
			String name = TextFormatting.UNDERLINE + StringHelper.translateToLocal("lucraftcore.info.suitabilities") + ": " + SuitSet.getSuitSet(player).getDisplayName();
			String desc = SuitSet.getSuitSet(player).getExtraDescription(ItemStack.EMPTY) != null ? SuitSet.getSuitSet(player).getExtraDescription(ItemStack.EMPTY).get(0) : null;
			int y = j + 10;

			if (desc != null && !desc.equalsIgnoreCase("")) {
				boolean unicode = mc.fontRenderer.getUnicodeFlag();
				mc.fontRenderer.setUnicodeFlag(true);
				int x1 = this.xSize / 2 - mc.fontRenderer.getStringWidth(desc) / 2;
				mc.fontRenderer.drawString(desc, i + x1, j + 16, 0x373737);
				mc.fontRenderer.setUnicodeFlag(unicode);
				j -= 3;
			}

			int x = this.xSize / 2 - mc.fontRenderer.getStringWidth(name) / 2;
			mc.fontRenderer.drawString(name, i + x, j + 10, 0x373737);
		}

		if (list != null) {
			this.list.drawScreen(mouseX, mouseY, partialTicks);

			GuiButton info = this.buttonList.get(0);
			if (selectedAbility >= 0 && info.enabled && mouseX >= info.x && mouseX <= info.x + info.width && mouseY >= info.y && mouseY <= info.y + info.height) {
				Ability ability = handler.getAbilities().get(selectedAbility);

				if (ability != null) {
					List<String> list = new ArrayList<String>();
					for (String s : ability.getDisplayDescription().split("\n")) {
						for (String s2 : mc.fontRenderer.listFormattedStringToWidth(s, 150)) {
							list.add(s2);
						}
					}
					if(mc.gameSettings.advancedItemTooltips)
						list.add(TextFormatting.DARK_GRAY + ability.getAbilityEntry().getRegistryName().toString());
					this.drawHoveringText(list, mouseX + 10, mouseY);
				}
			}
		}

	}

}
