package lucraft.mods.lucraftcore.superpowers.gui;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.network.MessageSuperpowerStyle;
import lucraft.mods.lucraftcore.util.container.ContainerDummy;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public abstract class GuiCustomizer extends GuiContainer {

	public static ResourceLocation DEFAULT_TEX = new ResourceLocation(LucraftCore.MODID, "textures/gui/customizer.png");
	
	public GuiCustomizer() {
		super(new ContainerDummy());
	}

	public abstract NBTTagCompound getStyleNBTTag();
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	public void sendStyleNBTTagToServer() {
		LCPacketDispatcher.sendToServer(new MessageSuperpowerStyle(getStyleNBTTag()));
	}
	
}