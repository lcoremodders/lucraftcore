package lucraft.mods.lucraftcore.superpowers.abilitybar;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilitybar.AbilityBarAbility.AbilityOrigin;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class AbilityBarMainHandler {

	public static ResourceLocation HUD_TEX = new ResourceLocation(LucraftCore.MODID, "textures/gui/ability_bar.png");
	public static Minecraft mc = Minecraft.getMinecraft();

	@SubscribeEvent
	public void onRenderGameHud(RenderGameOverlayEvent.Post e) {
		if (e.isCancelable() || e.getType() != ElementType.EXPERIENCE || !LCConfig.superpowers.showAbilityBar || mc.gameSettings.showDebugInfo) {
			return;
		}

		ArrayList<AbilityBarAbility> bars = new ArrayList<>();
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		SuperpowerPlayerHandler handler = SuperpowerHandler.getSuperpowerPlayerHandler(mc.player);
		SuitSetAbilityHandler suitSetHandler = SuperpowerHandler.getSuitSetAbilityHandler(mc.player);
		int length = 0;

		if (handler != null) {
			for (Ability ab : handler.getAbilities()) {
				AbilityBarAbility aba = new AbilityBarAbility(ab, AbilityOrigin.SUPERPOWER);
				if (aba.isActive(mc, mc.player, superpower, handler) && ab.showInAbilityBar() && !ab.isHidden()) {
					bars.add(aba);
					length += aba.getLength(mc, mc.player, superpower, handler);
				}
			}
		}

		if (suitSetHandler != null) {
			for (Ability ab : suitSetHandler.getAbilities()) {
				AbilityBarAbility aba = new AbilityBarAbility(ab, AbilityOrigin.SUIT);
				if (aba.isActive(mc, mc.player, superpower, suitSetHandler) && ab.showInAbilityBar() && !ab.isHidden()) {
					bars.add(aba);
					length += aba.getLength(mc, mc.player, superpower, suitSetHandler);
				}
			}
		}

		int size = bars.size();
		AbilityBarPos pos = LCConfig.superpowers.abilityBarPosition;
		int start = pos.centered ? (pos.vertical ? (e.getResolution().getScaledHeight() / 2 - length / 2) : (e.getResolution().getScaledWidth() / 2 - length / 2)) : 0;
		int startX = start;

		GlStateManager.pushMatrix();
		Tessellator tes = Tessellator.getInstance();
		BufferBuilder bb = tes.getBuffer();

		for (AbilityBarAbility ab : bars) {

			GlStateManager.color(1, 1, 1, 1);
			int abLength = ab.getLength(mc, mc.player, superpower, ab.origin == AbilityOrigin.SUPERPOWER ? handler : suitSetHandler);
			mc.renderEngine.bindTexture(HUD_TEX);

			if (pos.vertical) {
				// Vertical Slots
				GlStateManager.pushMatrix();

				if (pos.side == ScreenSide.RIGHT)
					GlStateManager.translate(e.getResolution().getScaledWidth() - 22, 0, 0);

				mc.ingameGUI.drawTexturedModalRect(0, startX, 0, 0, 22, 19);
				mc.ingameGUI.drawTexturedModalRect(0, startX + 19, 0, 4, 22, abLength - 21);
				mc.ingameGUI.drawTexturedModalRect(0, startX + 19 + abLength - 21, 0, 19, 22, 3);

				ab.render(mc, 3, startX + 3, mc.player, superpower, ab.origin == AbilityOrigin.SUPERPOWER ? handler : suitSetHandler);

				// Vertical Cooldowns
				if (ab.ability.renderCooldown()) {
					GlStateManager.pushMatrix();
					GlStateManager.disableTexture2D();
					GL11.glDisable(GL11.GL_CULL_FACE);
					Vec3d color = ab.ability.getCooldownBarColor();
					float percentage = ab.ability.getCooldownPercentage();

					GlStateManager.color(0, 0, 0);
					bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
					bb.pos(4, startX + 19 + abLength - 24, 0).endVertex();
					bb.pos(4 + 14, startX + 19 + abLength - 24, 0).endVertex();
					bb.pos(4 + 14, startX + 19 + abLength - 22, 0).endVertex();
					bb.pos(4, startX + 19 + abLength - 22, 0).endVertex();
					tes.draw();

					GlStateManager.color((float) color.x, (float) color.y, (float) color.z, 1);
					bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
					bb.pos(4, startX + 19 + abLength - 24, 0).endVertex();
					bb.pos(4 + percentage * 14, startX + 19 + abLength - 24, 0).endVertex();
					bb.pos(4 + percentage * 14, startX + 19 + abLength - 23, 0).endVertex();
					bb.pos(4, startX + 19 + abLength - 23, 0).endVertex();
					tes.draw();

					GlStateManager.enableTexture2D();
					GL11.glEnable(GL11.GL_CULL_FACE);
					GlStateManager.popMatrix();
				}

				// Vertical Keys
				String keyInfo = ab.getKeyInfo(mc, mc.player, superpower, ab.origin == AbilityOrigin.SUPERPOWER ? handler : suitSetHandler);

				if (keyInfo != null && !keyInfo.isEmpty()) {
					if (pos.side == ScreenSide.LEFT) {
						// Keys Left
						GlStateManager.pushMatrix();
						GlStateManager.disableTexture2D();
						GL11.glDisable(GL11.GL_CULL_FACE);
						GlStateManager.color(0.2F, 0.2F, 0.2F, 0.5F);
						int infoLength = mc.fontRenderer.getStringWidth(keyInfo);

						bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
						bb.pos(22, startX + 6, 0).endVertex();
						bb.pos(24 + infoLength + 6, startX + 6, 0).endVertex();
						bb.pos(24 + infoLength + 6, startX + 18, 0).endVertex();
						bb.pos(22, startX + 18, 0).endVertex();
						tes.draw();

						GlStateManager.enableTexture2D();
						GL11.glEnable(GL11.GL_CULL_FACE);
						GlStateManager.popMatrix();

						mc.ingameGUI.drawString(mc.fontRenderer, keyInfo, 27, startX + 8, 0xfefefe);
					} else if (pos.side == ScreenSide.RIGHT) {
						// Keys Right
						GlStateManager.pushMatrix();
						GlStateManager.disableTexture2D();
						GL11.glDisable(GL11.GL_CULL_FACE);
						GlStateManager.color(0.2F, 0.2F, 0.2F, 0.5F);
						int infoLength = mc.fontRenderer.getStringWidth(keyInfo);

						bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
						bb.pos(0, startX + 6, 0).endVertex();
						bb.pos(-2 - infoLength - 6, startX + 6, 0).endVertex();
						bb.pos(-2 - infoLength - 6, startX + 18, 0).endVertex();
						bb.pos(0, startX + 18, 0).endVertex();
						tes.draw();

						GlStateManager.enableTexture2D();
						GL11.glEnable(GL11.GL_CULL_FACE);
						GlStateManager.popMatrix();

						mc.ingameGUI.drawString(mc.fontRenderer, keyInfo, -5 - infoLength, startX + 8, 0xfefefe);
					}
				}
				GlStateManager.popMatrix();
			} else {
				// Horizontal Slots
				GlStateManager.pushMatrix();

				mc.ingameGUI.drawTexturedModalRect(startX, 0, 0, 0, 19, 22);
				mc.ingameGUI.drawTexturedModalRect(startX + 19, 0, 4, 0, abLength - 21, 22);
				mc.ingameGUI.drawTexturedModalRect(startX + 19 + abLength - 21, 0, 19, 0, 3, 22);

				ab.render(mc, startX + 3, 3, mc.player, superpower, ab.origin == AbilityOrigin.SUPERPOWER ? handler : suitSetHandler);

				// Horizontal Cooldowns
				if (ab.ability.renderCooldown()) {
					GlStateManager.pushMatrix();
					GlStateManager.disableTexture2D();
					GL11.glDisable(GL11.GL_CULL_FACE);
					Vec3d color = ab.ability.getCooldownBarColor();
					float percentage = ab.ability.getCooldownPercentage();

					GlStateManager.color(0, 0, 0);
					bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
					bb.pos(startX + 19 + abLength - 24, 4, 0).endVertex();
					bb.pos(startX + 19 + abLength - 24, 4 + 14, 0).endVertex();
					bb.pos(startX + 19 + abLength - 22, 4 + 14, 0).endVertex();
					bb.pos(startX + 19 + abLength - 22, 4, 0).endVertex();
					tes.draw();

					GlStateManager.color((float) color.x, (float) color.y, (float) color.z, 1);
					bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
					bb.pos(startX + 19 + abLength - 24, 4 + (1F - percentage) * 14, 0).endVertex();
					bb.pos(startX + 19 + abLength - 24, 4 + 14, 0).endVertex();
					bb.pos(startX + 19 + abLength - 23, 4 + 14, 0).endVertex();
					bb.pos(startX + 19 + abLength - 23, 4 + (1F - percentage) * 14, 0).endVertex();
					tes.draw();

					GlStateManager.enableTexture2D();
					GL11.glEnable(GL11.GL_CULL_FACE);
					GlStateManager.popMatrix();
				}

				// Vertical Keys
				String keyInfo = ab.getKeyInfo(mc, mc.player, superpower, ab.origin == AbilityOrigin.SUPERPOWER ? handler : suitSetHandler);
				
				if(mc.fontRenderer.getStringWidth(keyInfo) >= 12)
					keyInfo = keyInfo.substring(0, 1) + "...";
				
				if (keyInfo != null && !keyInfo.isEmpty()) {
					// Keys Down
					GlStateManager.pushMatrix();
					GlStateManager.disableTexture2D();
					GL11.glDisable(GL11.GL_CULL_FACE);
					GlStateManager.color(0.2F, 0.2F, 0.2F, 0.5F);
					int infoLength = mc.fontRenderer.getStringWidth(keyInfo);

					bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
					bb.pos(startX + 6, 22, 0).endVertex();
					bb.pos(startX + 6, 36, 0).endVertex();
					bb.pos(startX + 18, 36, 0).endVertex();
					bb.pos(startX + 18, 22, 0).endVertex();
					tes.draw();

					GlStateManager.enableTexture2D();
					GL11.glEnable(GL11.GL_CULL_FACE);
					GlStateManager.popMatrix();

					mc.ingameGUI.drawString(mc.fontRenderer, keyInfo, startX + 12 - infoLength / 2, 25, 0xfefefe);
				}

				GlStateManager.popMatrix();
			}
			startX += abLength;
		}

		GlStateManager.popMatrix();
	}

	public static enum AbilityBarPos {

		RIGHT_CENTER(true, true, ScreenSide.RIGHT), LEFT_CENTER(true, true, ScreenSide.LEFT), RIGHT_TOP_CORNER(true, false, ScreenSide.RIGHT), LEFT_TOP_CORNER(true, false, ScreenSide.LEFT), TOP(false, true, ScreenSide.TOP);

		public boolean vertical;
		public boolean centered;
		public ScreenSide side;

		private AbilityBarPos(boolean vertical, boolean centered, ScreenSide side) {
			this.vertical = vertical;
			this.centered = centered;
			this.side = side;
		}

	}

	public static enum ScreenSide {

		LEFT, RIGHT, TOP, BOTTOM;

	}

}
