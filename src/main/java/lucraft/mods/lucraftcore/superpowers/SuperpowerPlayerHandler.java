package lucraft.mods.lucraftcore.superpowers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability.AbilityComparator;
import lucraft.mods.lucraftcore.superpowers.abilities.IAbilityContainer;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.superpowers.toasts.SuperpowerLevelUpToast;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class SuperpowerPlayerHandler implements IAbilityContainer {

	private List<Ability> abilities = new ArrayList<Ability>();
	private int xp;
	private int level;
	private NBTTagCompound style;
	
	public final ISuperpowerCapability cap;
	public final Superpower superpower;
	
	public SuperpowerPlayerHandler(ISuperpowerCapability cap, Superpower superpower) {
		this.cap = cap;
		this.superpower = superpower;
	}
	
	public EntityPlayer getPlayer() {
		return cap.getPlayer();
	}
	
	public final void update(Phase phase) {
		if(phase == Phase.START) {
			for(Ability ability : abilities) {
				ability.onUpdate();
			}
		}
		
		onUpdate(phase);
	}
	
	public void onUpdate(Phase phase) {
		
	}
	
	public void onApplyPower() {
		
	}
	
	public void onRemove() {
		
	}
	
	@Override
	public List<Ability> getAbilities() {
		return abilities;
	}
	
	@Override
	public Ability getAbilityForKey(AbilityKeys key) {
		return null;
	}
	
	public int getLevel() {
		if(superpower.canLevelUp())
			return MathHelper.clamp(level, 1, superpower.getMaxLevel());
		else return 0;
	}
	
	public int getXP() {
		if(superpower.canLevelUp())
			return MathHelper.clamp(xp, 0, superpower.getXPForLevel(getLevel() + 1));
		else return 0;
	}
	
	public void setLevel(int level) {
		if(superpower.canLevelUp()) {
			this.level = MathHelper.clamp(level, 1, superpower.getMaxLevel());
			SuperpowerHandler.syncToPlayer(getPlayer());
		}
	}
	
	public void setXP(int xp) {
		if(superpower.canLevelUp()) {
			this.xp = MathHelper.clamp(xp, 0, superpower.getXPForLevel(getLevel() + 1));
			SuperpowerHandler.syncToPlayer(getPlayer());
		}
	}
	
	public void addXP(int xp) {
		addXP(xp, true);
	}
	
	public void addXP(int xp, boolean showMessage) {
		if(xp > 0 && superpower.canLevelUp() && level < superpower.getMaxLevel()) {
			int max = superpower.getXPForLevel(level + 1);
			this.setXP(getXP() + xp);
			int tB = getXP();
			
			if(showMessage)
				sendXPMessage(xp);
			
			if(getXP() >= max) {
				levelUp();
				addXP(tB - max, false);
			}
		}
	}
	
	public void levelUp() {
		if(superpower.canLevelUp() && level < superpower.getMaxLevel()) {
			this.setLevel(getLevel() + 1);
			this.setXP(0);
			this.onLevelUp(getLevel());
			
			SuperpowerHandler.syncToPlayer(getPlayer());
		}
	}
	
	public NBTTagCompound getStyleNBTTag() {
		return style;
	}
	
	public void setStyleNBTTag(NBTTagCompound tag) {
		this.style = tag;
		SuperpowerHandler.syncToAll(getPlayer());
	}
	
	@SideOnly(Side.CLIENT)
	public void sendXPMessage(int xp) {
		// TODO XP Message
	}
	
	@SideOnly(Side.CLIENT)
	public void sendLevelUpMessage(int level) {
		Minecraft.getMinecraft().getToastGui().add(new SuperpowerLevelUpToast());
	}
	
	public void onLevelUp(int newLevel) {
		if(getPlayer().world.isRemote)
			sendLevelUpMessage(newLevel);
	}
	
	/**
	 * 
	 * @param newSuperpower The new superpower that is about to override the current one. It can be null if the current superpower is supposed to be removed.
	 * @return Returns true if the current superpower can be overriden/removed
	 */
	public boolean canRemove(Superpower newSuperpower) {
		return true;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setInteger("Level", level);
		compound.setInteger("XP", xp);
		compound.setTag("Style", style);
		
		NBTTagList tagList = new NBTTagList();
		
		for(Ability ability : getAbilities()) {
			tagList.appendTag(ability.serializeNBT());
		}
		
		compound.setTag("Abilities", tagList);
		return compound;
	}
	
	public void readFromNBT(NBTTagCompound compound) {
		level = compound.getInteger("Level");
		xp = compound.getInteger("XP");
		
		if(compound.hasKey("Style"))
			style = compound.getCompoundTag("Style");
		else
			style = superpower.getDefaultStyleTag();
		
		if(compound.getTagList("Abilities", 10).tagCount() > 0) {
			List<Ability> list = new ArrayList<Ability>();
			NBTTagList tagList = compound.getTagList("Abilities", 10);
			
			list = superpower.getDefaultAbilities(getPlayer(), new ArrayList<>());
			
			for(int i = 0; i < tagList.tagCount(); i++) {
				NBTTagCompound nbt = tagList.getCompoundTagAt(i);
				Class<? extends Ability> clz = Ability.ABILITY_REGISTRY.containsKey(new ResourceLocation(nbt.getString("Ability"))) ? Ability.ABILITY_REGISTRY.getValue(new ResourceLocation(nbt.getString("Ability"))).getAbilityClass() : null;
				
				for(Ability ab : list) {
					if(ab.getClass() == clz) {
						ab.deserializeNBT(nbt);
					}
				}
			}
			
			Collections.sort(list, new AbilityComparator());
			this.abilities = list;
		} else {
			List<Ability> list = superpower.getDefaultAbilities(getPlayer(), new ArrayList<>());
			Collections.sort(list, new AbilityComparator());
			this.abilities = list;
		}
	}
	
}
