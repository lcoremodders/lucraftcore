package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

import java.lang.reflect.InvocationTargetException;

public class AbilityGenerator {

	public ResourceLocation loc;
	public JsonObject data;
	public int key;

	public AbilityGenerator(ResourceLocation loc, JsonObject data, int key) {
		this.loc = loc;
		this.data = data;
		this.key = key;
	}

	public Ability create(EntityPlayer player, IAbilityContainer container) {
		Ability ab = null;
		try {
			if(Ability.ABILITY_REGISTRY.containsKey(loc)) {
				ab = Ability.ABILITY_REGISTRY.getValue(loc).getAbilityClass().getConstructor(EntityPlayer.class).newInstance(player);
				ab.readFromAddonPack(data, container);
				ab.setKey(key);
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return ab;
	}

}
