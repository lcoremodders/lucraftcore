package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import lucraft.mods.lucraftcore.superpowers.JsonSuperpower;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AbilityTogglePower extends AbilityToggle {

    public List<ResourceLocation> abilities;
    public JsonSuperpower.SuperpowerIconType iconType = null;
    public ItemStack iconStack = null;
    public ResourceLocation iconLoc = null;

    public AbilityTogglePower(EntityPlayer player) {
        super(player);
    }

    public AbilityTogglePower(EntityPlayer player, ResourceLocation... abilities) {
        super(player);
        this.abilities = Arrays.asList(abilities);
    }

    @Override
    public void init(List<Ability> abilities) {
        for(Ability ab : abilities) {
            if(ab != this) {
                ab.setUnlocked(this.isEnabled());
            }
        }
    }

    @Override
    public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
        super.readFromAddonPack(data, abilities);
        JsonArray array = JsonUtils.getJsonArray(data, "abilities");
        this.abilities = new ArrayList<>();
        for(int i = 0; i < array.size(); i++) {
            this.abilities.add(new ResourceLocation(array.get(i).getAsString()));
        }

        if (JsonUtils.hasField(data, "icon")) {
            JsonObject icon = JsonUtils.getJsonObject(data, "icon");
            String type = JsonUtils.getString(icon, "type");
            this.iconType = type.equalsIgnoreCase("item") ? JsonSuperpower.SuperpowerIconType.ITEM : (type.equalsIgnoreCase("texture") ? JsonSuperpower.SuperpowerIconType.TEXTURE : null);

            if (this.iconType == JsonSuperpower.SuperpowerIconType.ITEM) {
                this.iconStack = deserializeIcon(JsonUtils.getJsonObject(icon, "item"));
            } else if (this.iconType == JsonSuperpower.SuperpowerIconType.TEXTURE) {
                this.iconLoc = new ResourceLocation(JsonUtils.getString(icon, "texture"));
            }
        } else {
            this.iconType = JsonSuperpower.SuperpowerIconType.ITEM;
            this.iconStack = new ItemStack(Items.REDSTONE);
        }
    }

    private static ItemStack deserializeIcon(JsonObject object) {
        if (!object.has("item")) {
            throw new JsonSyntaxException("Unsupported icon type, currently only items are supported (add 'item' key)");
        } else {
            Item item = JsonUtils.getItem(object, "item");
            int i = JsonUtils.getInt(object, "data", 0);
            ItemStack ret = new ItemStack(item, 1, i);
            ret.setTagCompound(net.minecraftforge.common.util.JsonUtils.readNBT(object, "nbt"));
            return ret;
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
        if (iconType == JsonSuperpower.SuperpowerIconType.ITEM) {
            float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
            Minecraft.getMinecraft().getRenderItem().zLevel = -100.5F;
            GlStateManager.pushMatrix();
            GlStateManager.translate(x, y, 0);
            Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(iconStack, 0, 0);
            GlStateManager.popMatrix();
            Minecraft.getMinecraft().getRenderItem().zLevel = zLevel;
        } else if (iconType == JsonSuperpower.SuperpowerIconType.TEXTURE) {
            GlStateManager.pushMatrix();
            GlStateManager.translate(x, y, 0);
            GlStateManager.scale(0.0625F, 0.0625F, 1);
            GlStateManager.color(1, 1, 1, 1);
            Minecraft.getMinecraft().renderEngine.bindTexture(iconLoc);
            gui.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
            GlStateManager.popMatrix();
        }
    }

    public List<Ability> getAbilities() {
        List<Ability> list = new ArrayList<>();

        for(Ability ab : Ability.getAbilityContainerFromContext(context, player).getAbilities()) {
            if(this.abilities.contains(ab.getAbilityEntry().getRegistryName())) {
                list.add(ab);
            }
        }

        return list;
    }

    @Override
    public void setEnabled(boolean enabled) {
        if(this.enabled != enabled && context != null) {
            for(Ability ab : getAbilities()) {
                ab.setUnlocked(enabled);
            }
        }
        super.setEnabled(enabled);
    }

    @Override
    public void updateTick() {

    }
}
