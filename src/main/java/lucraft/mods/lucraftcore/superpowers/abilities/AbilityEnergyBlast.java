package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.superpowers.entities.EntityEnergyBlast;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.sounds.LCSoundEvents;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityEnergyBlast extends AbilityAction {

	public int cooldown_;
	public float damage;
	public Vec3d color;
	
	public AbilityEnergyBlast(EntityPlayer player) {
		super(player);
	}
	
	public AbilityEnergyBlast(EntityPlayer player, int cooldown, float damage, Vec3d color) {
		super(player);
		this.cooldown_ = cooldown;
		this.damage = damage;
		this.color = color;
	}

	@Override
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		super.readFromAddonPack(data, abilities);
		
		this.cooldown_ = JsonUtils.getInt(data, "cooldown", 0);
		this.damage = JsonUtils.getFloat(data, "damage");
		JsonArray array = JsonUtils.getJsonArray(data, "color");
		this.color = new Vec3d(array.get(0).getAsDouble(), array.get(1).getAsDouble(), array.get(2).getAsDouble());
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		GlStateManager.color((float)color.x, (float)color.y, (float)color.z);
		LCRenderHelper.drawIcon(mc, gui, x, y, 0, 15);
		GlStateManager.color(1, 1, 1);
		LCRenderHelper.drawIcon(mc, gui, x, y, 0, 14);
	}
	
	@Override
	public boolean hasCooldown() {
		return this.cooldown_ > 0;
	}
	
	@Override
	public int getMaxCooldown() {
		return this.cooldown_;
	}
	
	@Override
	public boolean action() {
		if(!player.world.isRemote) {
	        EntityEnergyBlast entity = new EntityEnergyBlast(player.world, player, damage, color);
	        entity.shoot(player, player.rotationPitch, player.rotationYaw, 0.0F, 1.5F, 1.0F);
	        player.world.spawnEntity(entity);
	        PlayerHelper.playSoundToAll(player.world, player.posX, player.posY, player.posZ, 50, LCSoundEvents.ENERGY_BLAST, SoundCategory.PLAYERS);
		}
		return true;
	}

}
