package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.UUID;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityHealth extends AbilityAttributeModifier {

	public AbilityHealth(EntityPlayer player) {
		this(player, null, 0F, 0);
	}

	public AbilityHealth(EntityPlayer player, UUID uuid, float factor, int operation) {
		super(player, uuid, factor, operation);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		 LCRenderHelper.drawIcon(mc, gui, x, y, 0, 11);
	}

	@Override
	public IAttribute getAttribute() {
		return SharedMonsterAttributes.MAX_HEALTH;
	}

}