package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilitybar.AbilityBarMainHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower.SuitSetAbilityHandler;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistry.AddCallback;
import net.minecraftforge.registries.IForgeRegistryInternal;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryManager;

import java.util.*;

@EventBusSubscriber(modid = LucraftCore.MODID)
public abstract class Ability implements INBTSerializable<NBTTagCompound> {

	public static IForgeRegistry<AbilityEntry> ABILITY_REGISTRY;

	@SubscribeEvent
	public static void onRegisterNewRegistries(RegistryEvent.NewRegistry e) {
		ABILITY_REGISTRY = new RegistryBuilder<AbilityEntry>().setName(new ResourceLocation(LucraftCore.MODID, "ability")).setType(AbilityEntry.class).setIDRange(0, 2048).add(new AbilityRegistryCallback()).create();
	}

	@SubscribeEvent
	public static void registerAbilities(RegistryEvent.Register<AbilityEntry> e) {
		e.getRegistry().register(new AbilityEntry(AbilityTogglePower.class, new ResourceLocation(LucraftCore.MODID, "toggle_power")));
		e.getRegistry().register(new AbilityEntry(AbilityHealth.class, new ResourceLocation(LucraftCore.MODID, "health")));
		e.getRegistry().register(new AbilityEntry(AbilityHealing.class, new ResourceLocation(LucraftCore.MODID, "healing")));
		e.getRegistry().register(new AbilityEntry(AbilityStrength.class, new ResourceLocation(LucraftCore.MODID, "strength")));
		e.getRegistry().register(new AbilityEntry(AbilityPunch.class, new ResourceLocation(LucraftCore.MODID, "punch")));
		e.getRegistry().register(new AbilityEntry(AbilitySprint.class, new ResourceLocation(LucraftCore.MODID, "sprint")));
		e.getRegistry().register(new AbilityEntry(AbilityJumpBoost.class, new ResourceLocation(LucraftCore.MODID, "jump_boost")));
		e.getRegistry().register(new AbilityEntry(AbilityDamageResistance.class, new ResourceLocation(LucraftCore.MODID, "resistance")));
		e.getRegistry().register(new AbilityEntry(AbilityFallResistance.class, new ResourceLocation(LucraftCore.MODID, "fall_resistance")));
		e.getRegistry().register(new AbilityEntry(AbilityFireResistance.class, new ResourceLocation(LucraftCore.MODID, "fire_resistance")));
		e.getRegistry().register(new AbilityEntry(AbilityStepAssist.class, new ResourceLocation(LucraftCore.MODID, "step_assist")));
		e.getRegistry().register(new AbilityEntry(AbilitySizeChange.class, new ResourceLocation(LucraftCore.MODID, "size_change")));
		e.getRegistry().register(new AbilityEntry(AbilityTeleport.class, new ResourceLocation(LucraftCore.MODID, "teleport")));
		e.getRegistry().register(new AbilityEntry(AbilityKnockbackResistance.class, new ResourceLocation(LucraftCore.MODID, "knockback_resistance")));
		e.getRegistry().register(new AbilityEntry(AbilityPotionPunch.class, new ResourceLocation(LucraftCore.MODID, "potion_punch")));
		e.getRegistry().register(new AbilityEntry(AbilitySlowfall.class, new ResourceLocation(LucraftCore.MODID, "slowfall")));
		e.getRegistry().register(new AbilityEntry(AbilityEnergyBlast.class, new ResourceLocation(LucraftCore.MODID, "energy_blast")));
		e.getRegistry().register(new AbilityEntry(AbilityFirePunch.class, new ResourceLocation(LucraftCore.MODID, "fire_punch")));
		e.getRegistry().register(new AbilityEntry(AbilityFlight.class, new ResourceLocation(LucraftCore.MODID, "flight")));
		e.getRegistry().register(new AbilityEntry(AbilityWaterBreathing.class, new ResourceLocation(LucraftCore.MODID, "water_breathing")));
	}

	// -----------------------------------------------------------------------------------------

	protected final EntityPlayer player;
	protected boolean unlocked;
	protected boolean enabled;
	protected int cooldown;
	protected int maxCooldown;
	protected AbilityType type;
	protected int ticks;
	protected Ability dependendAbility;
	protected Superpower superpower;
	protected int requiredLevel = -1;
	protected boolean isHidden;
	protected AbilityEntry entry;
	protected int key;
	public EnumAbilityContext context;

	public Ability(EntityPlayer player) {
		this.player = player;
		this.enabled = true;
		this.cooldown = 0;
		this.maxCooldown = 0;
		this.type = AbilityType.ACTION;
		this.ticks = 0;

		if (getAbilityType() == AbilityType.ACTION)
			this.setCooldown(getMaxCooldown());
		else
			this.setCooldown(0);

		for (AbilityEntry entries : ABILITY_REGISTRY.getValues()) {
			if (entries.getAbilityClass() == this.getClass()) {
				entry = entries;
			}
		}
	}

	/**
	 * This method is called BEFORE the abilities are added to the IAbilityContainer which is why you should use the abilities-parameter instead of getting the abilities from method in Ability.class
	 * @param abilities
	 */
	public void init(List<Ability> abilities) {

	}

	public AbilityEntry getAbilityEntry() {
		return entry;
	}

	public String getUnlocalizedName() {
		return ABILITY_REGISTRY.getKey(getAbilityEntry()).getResourcePath();
	}

	public String getModId() {
		return ABILITY_REGISTRY.getKey(getAbilityEntry()).getResourceDomain();
	}

	public String getTranslationName() {
		return getModId() + ".abilities." + getUnlocalizedName() + ".name";
	}

	public String getTranslationDescription() {
		return getModId() + ".abilities." + getUnlocalizedName() + ".desc";
	}

	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		LCRenderHelper.drawIcon(mc, gui, x, y, 0, 4);
	}

	@SideOnly(Side.CLIENT)
	public void drawAdditionalInfo(Minecraft mc, Gui gui, int x, int y) {
		if (isEnabled() && (getAbilityType() == AbilityType.TOGGLE || getAbilityType() == AbilityType.HELD)) {
			mc.renderEngine.bindTexture(AbilityBarMainHandler.HUD_TEX);
			mc.ingameGUI.drawTexturedModalRect(x + 16, y + 16, 24, 0, 6, 6);
		}
	}

	@SideOnly(Side.CLIENT)
	public boolean renderCooldown() {
		return hasCooldown();
	}

	@SideOnly(Side.CLIENT)
	public float getCooldownPercentage() {
		return 1F - (float) getCooldown() / (float) getMaxCooldown();
	}

	@SideOnly(Side.CLIENT)
	public Vec3d getCooldownBarColor() {
		float f = getCooldownPercentage();
		return new Vec3d(1D - f * 1D, 0.7D * f, 0.06666D * f);
	}

	public String getDisplayName() {
		return StringHelper.translateToLocal(getTranslationName());
	}

	public String getDisplayDescription() {
		String superpower = getDependentSuperpower() != null ? "\n" + (SuperpowerHandler.getSuperpower(player) == getDependentSuperpower() ? TextFormatting.GREEN : TextFormatting.RED) + StringHelper.translateToLocal("lucraftcore.info.requiressuperpower").replace("%SUPERPOWER", getDependentSuperpower().getDisplayName()) : "";
		String dependency = getDependentAbility() == null ? "" : "\n" + (getDependentAbility().isUnlocked() ? TextFormatting.GREEN : TextFormatting.RED) + StringHelper.translateToLocal("lucraftcore.info.requiresability").replace("%ABILITY", getDependentAbility().getDisplayName());
		String level = SuperpowerHandler.hasSuperpower(player) ? (getRequiredLevel() > -1 ? "\n" + (SuperpowerHandler.getSuperpowerPlayerHandler(player).getLevel() >= getRequiredLevel() ? TextFormatting.GREEN : TextFormatting.RED) + StringHelper.translateToLocal("lucraftcore.info.requireslevel").replace("%LEVEL", getRequiredLevel() + "") : "") : "";

		if (isUnlocked())
			dependency = level = superpower = "";

		return TextFormatting.UNDERLINE + "" + TextFormatting.BOLD + getDisplayName() + "\n" + TextFormatting.RESET + StringHelper.translateToLocal(getTranslationDescription()) + superpower + dependency + level;
	}

	public boolean isUnlocked() {
		if (getDependentSuperpower() != null && SuperpowerHandler.getSuperpower(player) != getDependentSuperpower()) {
			return false;
		}

		boolean levelUnlocked = true;

		if (requiredLevel > 0 && SuperpowerHandler.getSuperpowerPlayerHandler(player) != null) {
			levelUnlocked = SuperpowerHandler.getSuperpowerPlayerHandler(player).getLevel() >= getRequiredLevel();
		}

		return (dependendAbility == null ? unlocked : (dependendAbility.isUnlocked() && unlocked)) && levelUnlocked;
	}

	public Ability setUnlocked(boolean unlocked) {
		if (!unlocked || dependendAbility == null)
			this.unlocked = unlocked;
		else
			this.unlocked = dependendAbility != null ? dependendAbility.isUnlocked() && unlocked : unlocked;
		this.enabled = false;
		SuperpowerHandler.syncToPlayer(player);
		return this;
	}

	public Ability getDependentAbility() {
		return dependendAbility;
	}

	public Ability setDependentAbility(Ability ability) {
		this.dependendAbility = ability;
		SuperpowerHandler.syncToPlayer(player);
		return this;
	}

	public Ability setDependentSuperpower(Superpower superpower) {
		this.superpower = superpower;
		return this;
	}

	public Superpower getDependentSuperpower() {
		return superpower;
	}

	public int getRequiredLevel() {
		return requiredLevel;
	}

	public Ability setRequiredLevel(int requiredLevel) {
		this.requiredLevel = requiredLevel;
		return this;
	}

	public boolean isEnabled() {
		if (type == AbilityType.ACTION)
			return false;
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		if(this.enabled != enabled) {
			this.enabled = enabled;
			SuperpowerHandler.syncToAll(player);
		}
	}

	public boolean hasCooldown() {
		return false;
	}

	public int getCooldown() {
		return MathHelper.clamp(cooldown, 0, getMaxCooldown());
	}

	public void setCooldown(int cooldown) {
		this.cooldown = MathHelper.clamp(cooldown, 0, getMaxCooldown());
		SuperpowerHandler.syncToPlayer(player);
	}

	public int getMaxCooldown() {
		return maxCooldown;
	}

	public void setMaxCooldown(int maxCooldown) {
		this.maxCooldown = maxCooldown;

		if (getAbilityType() == AbilityType.ACTION)
			this.setCooldown(getMaxCooldown());
		else
			this.setCooldown(0);
	}

	public boolean isCoolingdown() {
		return hasCooldown() && getCooldown() > 0 && (getAbilityType() == AbilityType.ACTION ? !isEnabled() : true);
	}

	public AbilityType getAbilityType() {
		return type;
	}

	public void setAbilityType(AbilityType type) {
		this.type = type;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean hidden) {
		this.isHidden = hidden;
	}

	public void onUpdate() {
		if (isUnlocked()) {
			if (getAbilityType() == AbilityType.CONSTANT) {
				ticks++;
				updateTick();
			} else if (isEnabled()) {
				if (ticks == 0)
					firstTick();
				ticks++;
				updateTick();

				if (hasCooldown() && getAbilityType() != AbilityType.ACTION) {
					if (getCooldown() >= getMaxCooldown())
						setEnabled(false);
					else
						setCooldown(getCooldown() + 1);
				}
			} else {
				if (ticks != 0) {
					lastTick();
					ticks = 0;
				}

				if (hasCooldown()) {
					if (getCooldown() > 0)
						this.setCooldown(getCooldown() - 1);
					else if (getAbilityType() == AbilityType.ACTION)
						setEnabled(true);
				}
			}
		} else if (ticks != 0) {
			lastTick();
			ticks = 0;
		}
	}

	public void firstTick() {

	}

	public void updateTick() {

	}

	public void lastTick() {

	}

	public boolean action() {
		return false;
	}

	/**
	 * Used for json-generated abilities. No effect for other uses
	 * @return
	 */
	public int getKey() {
		return key;
	}
	
	/**
	 * Used for json-generated abilities. No effect for other uses
	 * @return
	 */
	public void setKey(int key) {
		this.key = key;
	}
	
	public boolean checkConditions() {
		return true;
	}

	public boolean showInAbilityBar() {
		return true;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setString("Ability", getAbilityEntry().getRegistryName().toString());
		nbt.setBoolean("Unlocked", unlocked);
		nbt.setBoolean("Enabled", enabled);
		nbt.setInteger("Cooldown", cooldown);
		nbt.setInteger("MaxCooldown", maxCooldown);
		nbt.setInteger("Ticks", ticks);
		nbt.setBoolean("Hidden", isHidden);
		return nbt;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		this.unlocked = nbt.getBoolean("Unlocked");
		this.enabled = nbt.getBoolean("Enabled");
		this.cooldown = nbt.getInteger("Cooldown");
		this.maxCooldown = nbt.getInteger("MaxCooldown");
		this.ticks = nbt.getInteger("Ticks");
		this.isHidden = nbt.getBoolean("Hidden");
	}

	public void onAttacked(LivingAttackEvent e) {

	}

	public void onPlayerHurt(LivingHurtEvent e) {

	}

	public void onHurt(LivingHurtEvent e) {

	}

	public void onAttackEntity(AttackEntityEvent e) {

	}

	public void onBreakSpeed(BreakSpeed e) {

	}
	
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		setUnlocked(true);
		if(JsonUtils.hasField(data, "required_level"))
			setRequiredLevel(JsonUtils.getInt(data, "required_level"));
		if(JsonUtils.hasField(data, "required_ability")) {
			for(Ability ab : abilities.getAbilities()) {
				if(ab.getAbilityEntry().getRegistryName().equals(new ResourceLocation(JsonUtils.getString(data, "required_ability")))) {
					this.setDependentAbility(ab);
				}
			}
		}
		if(JsonUtils.hasField(data, "required_superpower"))
			setDependentSuperpower(SuperpowerHandler.SUPERPOWER_REGISTRY.getValue(new ResourceLocation(JsonUtils.getString(data, "required_superpower"))));
	}

	@Deprecated
	public static <T extends Ability> T getAbilityFromClass(List<Ability> list, Class<T> abilityClass) {
		for (Ability ab : list) {
			if (ab.getClass() == abilityClass) {
				return (T) ab;
			}
		}

		return null;
	}

	public static <T extends Ability> List<T> getAbilitiesFromClass(List<Ability> list, Class<T> abilityClass) {
		List<T> abilities = new ArrayList<>();
		for(Ability ab : list) {
			if(ab.getClass() == abilityClass) {
				abilities.add((T)ab);
			}
		}

		return abilities;
	}

	public static List<Ability> getCurrentPlayerAbilities(EntityPlayer player) {
		ArrayList<Ability> list = new ArrayList<>();
		SuitSetAbilityHandler suitHandler = SuperpowerHandler.getSuitSetAbilityHandler(player);
		SuperpowerPlayerHandler handler = SuperpowerHandler.getSuperpowerPlayerHandler(player);

		if (suitHandler != null)
			list.addAll(suitHandler.getAbilities());
		if (handler != null)
			list.addAll(handler.getAbilities());

		return list;
	}

	public static boolean hasPlayerAbility(EntityPlayer player, Class<? extends Ability> ability) {
		for(Ability ab : getCurrentPlayerAbilities(player)) {
			if(ab.getClass() == ability) {
				return true;
			}
		}
		return false;
	}

	public static AbilityEntry getAbilityEntryFromClass(Class<? extends Ability> clazz) {
		for (AbilityEntry entries : ABILITY_REGISTRY.getValues()) {
			if (entries.getAbilityClass().equals(clazz)) {
				return entries;
			}
		}

		return null;
	}

	public static boolean isAbilityEnabled(Ability ability) {
		return isAbilityEnabled(ability.getClass());
	}

	public static boolean isAbilityEnabled(Class<? extends Ability> clz) {
		List<ResourceLocation> list = new ArrayList<>();
		for (String s : LCConfig.superpowers.disabledAbilities)
			list.add(new ResourceLocation(s));
		return !list.contains(ABILITY_REGISTRY.getKey(Ability.getAbilityEntryFromClass(clz)));
	}

	public static List<Ability> removeDisabledAbilities(List<Ability> list) {
		List<Ability> newList = new ArrayList<>();

		for (Ability ab : list) {
			boolean isDisabled = false;
			Ability ab1 = ab;

			while (ab1.getDependentAbility() != null) {
				ab1 = ab1.getDependentAbility();

				if (!isAbilityEnabled(ab1))
					isDisabled = true;
			}

			if (isAbilityEnabled(ab) && !isDisabled) {
				newList.add(ab);
			}
		}

		return newList;
	}

	public static IAbilityContainer getAbilityContainerFromContext(EnumAbilityContext context, EntityPlayer player) {
		return context == EnumAbilityContext.SUPERPOWER ? SuperpowerHandler.getSuperpowerPlayerHandler(player) : SuperpowerHandler.getSuitSetAbilityHandler(player);
	}
	
	public static enum AbilityType {
		ACTION, HELD, TOGGLE, CONSTANT;
	}
	
	public static enum EnumAbilityContext {
		SUPERPOWER, SUIT;
	}

	public static class AbilityComparator implements Comparator<Ability> {

		@Override
		public int compare(Ability a1, Ability a2) {
			int id1 = AbilityRegistryCallback.SORTING_IDS_BY_ENTRY.get(a1.getAbilityEntry().getRegistryName());
			int id2 = AbilityRegistryCallback.SORTING_IDS_BY_ENTRY.get(a2.getAbilityEntry().getRegistryName());

			if (id1 > id2)
				return 1;
			else if (id1 < id2)
				return -1;

			return 0;
		}

	}

	public static class AbilityEntry extends net.minecraftforge.registries.IForgeRegistryEntry.Impl<AbilityEntry> {

		private Class<? extends Ability> clazz;

		public AbilityEntry(Class<? extends Ability> clazz, ResourceLocation registryName) {
			this.clazz = clazz;
			this.setRegistryName(registryName);
		}

		public Class<? extends Ability> getAbilityClass() {
			return clazz;
		}

	}

	public static class AbilityRegistryCallback implements AddCallback<AbilityEntry> {

		public static Map<Integer, ResourceLocation> SORTING_IDS_BY_ID = new HashMap<Integer, ResourceLocation>();
		public static Map<ResourceLocation, Integer> SORTING_IDS_BY_ENTRY = new HashMap<ResourceLocation, Integer>();
		public static int id = 0;

		@Override
		public void onAdd(IForgeRegistryInternal<AbilityEntry> owner, RegistryManager stage, int id, AbilityEntry obj, AbilityEntry oldObj) {
			if (!SORTING_IDS_BY_ENTRY.containsKey(obj.getRegistryName())) {
				SORTING_IDS_BY_ID.put(AbilityRegistryCallback.id, obj.getRegistryName());
				SORTING_IDS_BY_ENTRY.put(obj.getRegistryName(), AbilityRegistryCallback.id);
				AbilityRegistryCallback.id++;
			}
		}

	}

}