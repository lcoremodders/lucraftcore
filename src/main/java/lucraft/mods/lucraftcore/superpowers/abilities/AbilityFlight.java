package lucraft.mods.lucraftcore.superpowers.abilities;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.ModuleSuperpowers;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityFlight extends AbilityToggle {

	public AbilityFlight(EntityPlayer player) {
		super(player);
	}

	@Override
	public void firstTick() {
		super.firstTick();
	}

	@Override
	public void updateTick() {
		if (player.onGround && ticks > 20)
			this.setEnabled(false);

		if (player.moveForward > 0F && !player.onGround) {
			Vec3d vec = player.getLookVec();
			float speed = player.isSprinting() ? 5F : 1F;
			player.motionX = vec.x * speed;
			player.motionY = vec.y * speed;
			player.motionZ = vec.z * speed;

			if (!player.world.isRemote) {
				player.fallDistance = 0.0F;
			}
		} else {
			float motionY = 0F;
			if (ticks < 20) {
				int lowestY = player.getPosition().getY();

				while (lowestY > 0 && !player.world.isBlockFullCube(new BlockPos(player.posX, lowestY, player.posZ))) {
					lowestY--;
				}

				if (player.getPosition().getY() - lowestY < 5) {
					motionY += 1;
				}
			}

			motionY += Math.sin(player.ticksExisted / 10F) / 100F;
			player.fallDistance = 0F;
			player.motionY = motionY;
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		LCRenderHelper.drawIcon(mc, gui, x, y, 1, 0);
	}

	@EventBusSubscriber(modid = LucraftCore.MODID, value = Side.CLIENT)
	public static class Renderer {

		@SubscribeEvent
		public static void onRenderModel(RenderModelEvent e) {
			if (!ModuleSuperpowers.INSTANCE.isEnabled())
				return;

			if (e.getEntityLiving() instanceof EntityPlayer) {
				for (AbilityFlight ab : Ability.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities((EntityPlayer) e.getEntityLiving()), AbilityFlight.class)) {
					if (ab != null && ab.isUnlocked() && ab.isEnabled()) {
						EntityPlayer player = (EntityPlayer) e.getEntityLiving();
						float speed = (float) MathHelper.clamp(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ) + (player.prevPosY - player.posY) * (player.prevPosY - player.posY)), 0F, 1F);
						GlStateManager.rotate(speed * (90F + player.rotationPitch), 1, 0, 0);
						break;
					}
				}
			}
		}

		@SubscribeEvent
		public static void onSetUpModel(RenderModelEvent.SetRotationAngels e) {
			if (!ModuleSuperpowers.INSTANCE.isEnabled())
				return;

			if (e.getEntity() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntity();
				for (AbilityFlight ab : Ability.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities((EntityPlayer) e.getEntity()), AbilityFlight.class)) {
					if (ab != null && ab.isUnlocked() && ab.isEnabled() && player.moveForward > 0F) {
						float speed = (float) MathHelper.clamp(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ) + (player.prevPosY - player.posY) * (player.prevPosY - player.posY)), 0F, 1F);
						e.setCanceled(true);
						float rotation = speed * (90F + player.rotationPitch);
						e.model.bipedHead.rotateAngleX -= Math.toRadians(rotation);
						e.model.bipedHeadwear.rotateAngleX = e.model.bipedHead.rotateAngleX;

						e.model.bipedRightArm.rotateAngleX = (float) Math.toRadians(180F);
						e.model.bipedLeftArm.rotateAngleX = e.model.bipedRightArm.rotateAngleX;
						e.model.bipedRightLeg.rotateAngleX = 0;
						e.model.bipedLeftLeg.rotateAngleX = 0;

						if (e.model instanceof ModelPlayer) {
							ModelPlayer model = (ModelPlayer) e.model;
							model.bipedRightArmwear.rotateAngleX = model.bipedRightArm.rotateAngleX;
							model.bipedLeftArmwear.rotateAngleX = model.bipedLeftArm.rotateAngleX;
							model.bipedRightLegwear.rotateAngleX = model.bipedRightLeg.rotateAngleX;
							model.bipedLeftLegwear.rotateAngleX = model.bipedLeftLeg.rotateAngleX;
						}
					}
				}
			}
		}

	}

}