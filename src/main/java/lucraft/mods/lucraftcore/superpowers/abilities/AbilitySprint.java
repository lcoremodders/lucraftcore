package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.UUID;

import lucraft.mods.lucraftcore.util.attributes.LCAttributes;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilitySprint extends AbilityAttributeModifier {

	public AbilitySprint(EntityPlayer player) {
		this(player, null, 1F);
	}

	public AbilitySprint(EntityPlayer player, UUID uuid, float factor) {
		super(player, uuid, factor, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		 LCRenderHelper.drawIcon(mc, gui, x, y, 0, 3);
	}

	@Override
	public IAttribute getAttribute() {
		return LCAttributes.SPRINT_SPEED;
	}

}