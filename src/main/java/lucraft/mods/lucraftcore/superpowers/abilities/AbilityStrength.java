package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.UUID;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityStrength extends AbilityAttributeModifier {

	public AbilityStrength(EntityPlayer player) {
		this(player, null, 1F, 0);
	}

	public AbilityStrength(EntityPlayer player, UUID uuid, float factor, int operation) {
		super(player, uuid, factor, operation);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		 LCRenderHelper.drawIcon(mc, gui, x, y, 0, 1);
	}

	@Override
	public IAttribute getAttribute() {
		return SharedMonsterAttributes.ATTACK_DAMAGE;
	}

}