package lucraft.mods.lucraftcore.superpowers.abilities;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.UUID;

public class AbilitySizeChange extends AbilityToggle {

	private UUID uuid;
	private float size;

	public AbilitySizeChange(EntityPlayer player) {
		super(player);
	}

	public AbilitySizeChange(EntityPlayer player, UUID uuid, float size) {
		super(player);
		this.uuid = uuid;
		this.size = size;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		 LCRenderHelper.drawIcon(mc, gui, x, y, 0, 10);
	}

	public float getSize() {
		return size;
	}

	public UUID getUUID() {
		return uuid;
	}

	@Override
	public boolean action() {
		if (isEnabled()) {
			//player.getEntityAttribute(LCAttributes.SIZE).removeModifier(getUUID());
		} else {
			//player.getEntityAttribute(LCAttributes.SIZE).applyModifier(new AttributeModifier(getUUID(), getUnlocalizedName(), getSize(), 0));
		}
		return true;
	}

	@Override
	public void updateTick() {

	}

	@Override
	public boolean isEnabled() {
		return false;
		//return player.getEntityAttribute(LCAttributes.SIZE).getModifier(getUUID()) != null;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		super.deserializeNBT(nbt);
		this.size = nbt.getFloat("Size");
		this.uuid = UUID.fromString(nbt.getString("UUID"));
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setFloat("Size", size);
		nbt.setString("UUID", uuid.toString());
		return nbt;
	}

}