package lucraft.mods.lucraftcore.superpowers.abilities;

import net.minecraft.entity.player.EntityPlayer;

public abstract class AbilityConstant extends Ability {

	public AbilityConstant(EntityPlayer player) {
		super(player);
		this.setAbilityType(AbilityType.CONSTANT);
	}

	@Override
	public abstract void updateTick();

}
