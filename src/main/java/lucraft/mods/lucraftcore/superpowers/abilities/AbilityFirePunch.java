package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.Random;

import com.google.gson.JsonObject;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityFirePunch extends AbilityToggle {

	protected int duration;
	protected int cooldown_;

	public AbilityFirePunch(EntityPlayer player) {
		super(player);
	}

	public AbilityFirePunch(EntityPlayer player, int duration, int cooldown) {
		super(player);
		this.duration = duration;
		this.cooldown_ = cooldown;
	}

	@Override
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		super.readFromAddonPack(data, abilities);
		this.duration = JsonUtils.getInt(data, "duration");
		this.cooldown_ = JsonUtils.getInt(data, "cooldown", 0);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		ItemStack stack = new ItemStack(Items.BLAZE_POWDER);
		float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
		Minecraft.getMinecraft().getRenderItem().zLevel = -100.5F;
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(stack, 0, 0);
		GlStateManager.popMatrix();
		Minecraft.getMinecraft().getRenderItem().zLevel = zLevel;
	}
	
	@Override
	public boolean hasCooldown() {
		return cooldown_ > 0;
	}
	
	@Override
	public int getMaxCooldown() {
		return cooldown_;
	}
	
	@Override
	public void updateTick() {
		Random rand = new Random();
		for (int j = 0; j < 1; ++j) {
			this.player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX + (rand.nextDouble() - 0.5D) * (double) player.width, player.posY + rand.nextDouble() * (double) player.height, player.posZ + (rand.nextDouble() - 0.5D) * (double) player.width, 0, 0, 0);
		}
	}
	@Override
	public void onHurt(LivingHurtEvent e) {
		if(isEnabled()) {
			e.getEntityLiving().setFire(duration);
		}
	}

}
