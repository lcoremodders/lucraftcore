package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityTeleport extends AbilityAction {

	protected float distance = 1;
	protected int cooldown_ = 0;

	public AbilityTeleport(EntityPlayer player) {
		super(player);
	}

	public AbilityTeleport(EntityPlayer player, float distance, int cooldown) {
		super(player);
		this.distance = distance;
		this.cooldown_ = cooldown;
	}

	@Override
	public void readFromAddonPack(JsonObject data, IAbilityContainer abilities) {
		super.readFromAddonPack(data, abilities);
		this.distance = JsonUtils.getFloat(data, "distance");
		this.cooldown_ = JsonUtils.getInt(data, "cooldown");
	}

	@Override
	public boolean hasCooldown() {
		return cooldown_ > 0;
	}

	@Override
	public int getMaxCooldown() {
		return cooldown_;
	}

	@Override
	public String getDisplayDescription() {
		return super.getDisplayDescription() + "\n \n" + TextFormatting.BLUE + (StringHelper.translateToLocal("lucraftcore.info.distance").replaceAll("%s", distance + ""));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		float zLevel = Minecraft.getMinecraft().getRenderItem().zLevel;
		Minecraft.getMinecraft().getRenderItem().zLevel = -100.5F;
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y, 0);
		Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(new ItemStack(Items.ENDER_PEARL), 0, 0);
		GlStateManager.popMatrix();
		Minecraft.getMinecraft().getRenderItem().zLevel = zLevel;
	}

	@Override
	public boolean action() {
		Vec3d lookVec = player.getLookVec().scale(distance);
		RayTraceResult rtr = player.world.rayTraceBlocks(new Vec3d(player.posX, player.posY + player.eyeHeight, player.posZ), new Vec3d(player.posX + lookVec.x, player.posY + player.eyeHeight + lookVec.y, player.posZ + lookVec.z));
		
		if (rtr != null && rtr.hitVec != null) {
			PlayerHelper.playSoundToAll(player.world, player.posX, player.posY, player.posZ, 50, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS);
			player.setPositionAndUpdate(rtr.hitVec.x, rtr.hitVec.y, rtr.hitVec.z);
			PlayerHelper.playSoundToAll(player.world, player.posX, player.posY, player.posZ, 50, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.PLAYERS);
			for (int i = 0; i < 30; ++i) {
				PlayerHelper.spawnParticleForAll(player.world, 50, EnumParticleTypes.PORTAL, true, (float) player.posX, (float) player.posY + player.world.rand.nextFloat() * (float) player.height, (float) player.posZ, (player.world.rand.nextFloat() - 0.5F) * 2.0F, -player.world.rand.nextFloat(), (player.world.rand.nextFloat() - 0.5F) * 2.0F, 1, 10);
			}
			return true;
		}
		return false;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		super.deserializeNBT(nbt);
		this.distance = nbt.getFloat("Distance");
		this.cooldown_ = nbt.getInteger("Cooldown_");
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setFloat("Distance", distance);
		nbt.setInteger("Cooldown_", cooldown_);
		return nbt;
	}

}
