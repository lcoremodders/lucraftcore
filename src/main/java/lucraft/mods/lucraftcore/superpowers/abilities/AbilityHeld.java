package lucraft.mods.lucraftcore.superpowers.abilities;

import net.minecraft.entity.player.EntityPlayer;

public abstract class AbilityHeld extends Ability {

	public AbilityHeld(EntityPlayer player) {
		super(player);
		this.setEnabled(false);
		this.setAbilityType(AbilityType.HELD);
	}

	@Override
	public abstract void updateTick();

}