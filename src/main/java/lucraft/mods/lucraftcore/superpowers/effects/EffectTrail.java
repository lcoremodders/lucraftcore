package lucraft.mods.lucraftcore.superpowers.effects;

import java.util.LinkedList;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EffectTrail extends Effect {

	public TrailType type;
	public Vec3d color;
	public boolean useSuitTrails;
	
	@Override
	public void readSettings(JsonObject json) {
		this.type = TrailType.getTrailTypeFromName(JsonUtils.getString(json, "trail_type"));
		if (type == null) {
			try {
				throw new Exception("The trail type '" + JsonUtils.getString(json, "trail_type") + "' doesn't exist!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		JsonArray array = JsonUtils.getJsonArray(json, "color");
		this.color = new Vec3d(array.get(0).getAsDouble(), array.get(1).getAsDouble(), array.get(2).getAsDouble());
		
		this.useSuitTrails = JsonUtils.getBoolean(json, "use_suit_trails", false);
	}
	
	public Vec3d getColor(EntityPlayer player) {
		if(!useSuitTrails)
			return color;
		else {
			SuitSet suitSet = SuitSet.getSuitSet(player);
			if(suitSet != null && suitSet.getData() != null && suitSet.getData().hasKey("trail")) {
				NBTTagCompound data = suitSet.getData().getCompoundTag("trail");
				return new Vec3d(data.getDouble("red"), data.getDouble("green"), data.getDouble("blue"));
			}
			
			return color;
		}
	}
	
	public static boolean hasTrail(EntityPlayer player) {
		for(EffectTrail trails : EffectHandler.getEffectsByClass(player, EffectTrail.class)) {
			if(EffectHandler.canEffectBeDisplayed(trails, player)) {
				return true;
			}
		}
		
		return false;
	}

	public static enum TrailType {

		LIGHTNINGS;

		@SideOnly(Side.CLIENT)
		public TrailRenderer getTrailRenderer() {
			if(this == LIGHTNINGS)
				return new TrailRendererLightnings();
			return null;
		}
		
		public static TrailType getTrailTypeFromName(String name) {
			for (TrailType types : values()) {
				if (types.toString().equalsIgnoreCase(name)) {
					return types;
				}
			}
			return null;
		}

	}
	
	public static class EntityTrail extends EntityLivingBase {

		public EntityPlayer owner;
		public EffectTrail[] effects;
		public float[] lightningFactor;
		
		public EntityTrail(World worldIn) {
			super(worldIn);
			noClip = true;
			ignoreFrustumCheck = true;
		}
		
		public EntityTrail(World world, EntityPlayer owner, EffectTrail... effects) {
			this(world);
			this.owner = owner;
			this.effects = effects;
			this.setSize(owner.width, owner.height);
			this.setLocationAndAngles(owner.posX, owner.posY, owner.posZ, owner.rotationYaw, owner.rotationPitch);
			
			this.swingProgress = owner.swingProgress;
			this.prevSwingProgress = owner.swingProgress;
			this.prevRenderYawOffset = owner.renderYawOffset;
			this.renderYawOffset = owner.renderYawOffset;
			this.prevRotationYawHead = owner.rotationYawHead;
			this.rotationYawHead = owner.rotationYawHead;
			this.prevRotationPitch = owner.rotationPitch;
			this.rotationPitch = owner.rotationPitch;
			this.limbSwingAmount = owner.limbSwingAmount;
			this.prevLimbSwingAmount = owner.limbSwingAmount;
			this.limbSwing = owner.limbSwing;
			
			this.lightningFactor = new float[20];
			for(int i = 0; i < 20; i++) {
				this.lightningFactor[i] = rand.nextFloat();
			}
		}

		public Random getRandom() {
			return new Random(this.getEntityId());
		}
		
		public Vec3d getLightningPosVector(int i) {
			float halfWidth = width / 2;
			return new Vec3d(posX - halfWidth + (lightningFactor[i] * width), posY, posZ - halfWidth + (lightningFactor[10 + i] * width));
		}
		
		@Override
		public void onUpdate() {
			super.onUpdate();
			
			if(this.ticksExisted >= 11) {
				this.setDead();
				SuperpowerHandler.getSuperpowerData(owner).removeTrailEntity(this);
			}
		}
		
		@Override
		public boolean shouldRenderInPass(int pass) {
			return pass == 1;
		}

		@Override
		public boolean canBeCollidedWith() {
			return false;
		}

		@Override
		public boolean canBePushed() {
			return false;
		}
		
		@Override
		public boolean writeToNBTOptional(NBTTagCompound par1NBTTagCompound) {
			return false;
		}
		
		@Override
		public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		}

		@Override
		public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
		}
		
		@Override
		public Iterable<ItemStack> getArmorInventoryList() {
			return this.owner.getArmorInventoryList();
		}

		@Override
		public ItemStack getItemStackFromSlot(EntityEquipmentSlot slotIn) {
			return this.owner.getItemStackFromSlot(slotIn);
		}

		@Override
		public void setItemStackToSlot(EntityEquipmentSlot slotIn, ItemStack stack) {
			
		}

		@Override
		public EnumHandSide getPrimaryHand() {
			return owner.getPrimaryHand();
		}
		
	}
	
	@SideOnly(Side.CLIENT)
	public static abstract class TrailRenderer {
		
		@SideOnly(Side.CLIENT)
		public abstract void renderTrail(EntityPlayer player, EffectTrail trail, LinkedList<EntityTrail> trailEntities);
		
	}

	@SideOnly(Side.CLIENT)
	public static class TrailRendererLightnings extends TrailRenderer {

		public static int lineWidth = 5;
		public static int innerLineWidth = 1;
		
		@Override
		public void renderTrail(EntityPlayer player, EffectTrail trail, LinkedList<EntityTrail> trailEntities) {
			if (Minecraft.getMinecraft().gameSettings.thirdPersonView == 0 && player == Minecraft.getMinecraft().player)
				return;

			GlStateManager.pushMatrix();
			GlStateManager.disableTexture2D();
			GlStateManager.disableLighting();
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
			GlStateManager.blendFunc(770, 1);
			LCRenderHelper.setLightmapTextureCoords(240, 240);
			EntityPlayer mcPlayer = Minecraft.getMinecraft().player;
			translateRendering(mcPlayer, player);

			int amountOfLightnings = 6;
			float lightningSpace = player.height / amountOfLightnings;

			for (int j = 0; j < amountOfLightnings; j++) {
				// 10 Blitze ----------------------------------------------
				Vec3d add = new Vec3d(0, j * lightningSpace, 0);
				float differ = 0.425F * (player.height / 1.8F);
				if (trailEntities.size() > 0) {

					Vec3d firstStart = (trailEntities.getLast().getLightningPosVector(j).subtract(trailEntities.getLast().getPositionEyes(LCRenderHelper.renderTick))).add((player.getPositionEyes(LCRenderHelper.renderTick).addVector(0.0D, -1.62F * (player.height / 1.8F), 0.0D)));
					Vec3d firstEnd = trailEntities.getLast().getLightningPosVector(j);
					float a = 1F - (trailEntities.getLast().ticksExisted + LCRenderHelper.renderTick) / 10F;
					drawLine(firstStart.add(add).addVector(0, player.height, 0), firstEnd.add(add.addVector(0, trailEntities.getLast().lightningFactor[j] * differ, 0)), lineWidth, innerLineWidth, trail.getColor(player), a);

					for (int i = 0; i < trailEntities.size(); i++) {
						if (i < (trailEntities.size() - 1)) {
							EntityTrail speedMirage = trailEntities.get(i);
							EntityTrail speedMirage2 = trailEntities.get(i + 1);
							Vec3d start = speedMirage.getLightningPosVector(j);
							Vec3d end = speedMirage2.getLightningPosVector(j);
							float progress = 1F - (speedMirage.ticksExisted + LCRenderHelper.renderTick) / 10F;
							drawLine(start.add(add.addVector(0, speedMirage.lightningFactor[j] * differ, 0)), end.add(add.addVector(0, speedMirage2.lightningFactor[j] * differ, 0)), 5, 1, trail.getColor(player), progress);
						}
					}
				}
			}
			GlStateManager.color(1, 1, 1, 1);
			LCRenderHelper.restoreLightmapTextureCoords();
			GlStateManager.disableBlend();
			GlStateManager.enableLighting();
			GlStateManager.enableTexture2D();
			GlStateManager.popMatrix();
		}
		
		@SideOnly(Side.CLIENT)
		public void drawLine(Vec3d start, Vec3d end, float lineWidth, float innerLineWidth, Vec3d color, float alpha) {
			if(start == null || end == null)
				return;
			
//			LCRenderHelper.setupRenderLightning();
//			LCRenderHelper.drawGlowingLine(start, end, 0.005F, color, 1F);
//			LCRenderHelper.finishRenderLightning();
			
			Tessellator tes = Tessellator.getInstance();
			BufferBuilder wr = tes.getBuffer();

			if (lineWidth > 0) {
				GlStateManager.color((float) color.x, (float) color.y, (float) color.z, alpha);
				GL11.glLineWidth(lineWidth);
				wr.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION);
				wr.pos(start.x, start.y, start.z).endVertex();
				wr.pos(end.x, end.y, end.z).endVertex();
				tes.draw();
			}

			if (innerLineWidth > 0) {
				GlStateManager.color(1, 1, 1, MathHelper.clamp(alpha - 0.2F, 0, 1));
				GL11.glLineWidth(innerLineWidth);
				wr.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION);
				wr.pos(start.x, start.y, start.z).endVertex();
				wr.pos(end.x, end.y, end.z).endVertex();
				tes.draw();
			}
		}
		
		@SideOnly(Side.CLIENT)
		public float median(double currentPos, double prevPos) {
			return (float) (prevPos + (currentPos - prevPos) * LCRenderHelper.renderTick);
		}

		@SideOnly(Side.CLIENT)
		public void translateRendering(EntityPlayer player, Entity entity) {
			double x = -median(entity.posX, entity.prevPosX) - (median(player.posX, player.prevPosX) - median(entity.posX, entity.prevPosX));
			double y = -median(entity.posY, entity.prevPosY) - (median(player.posY, player.prevPosY) - median(entity.posY, entity.prevPosY));
			double z = -median(entity.posZ, entity.prevPosZ) - (median(player.posZ, player.prevPosZ) - median(entity.posZ, entity.prevPosZ));
			GL11.glTranslatef((float) x, (float) y, (float) z);
		}
		
	}
	
}
