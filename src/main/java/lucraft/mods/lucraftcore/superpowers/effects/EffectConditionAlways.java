package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonObject;

import net.minecraft.entity.player.EntityPlayer;

public class EffectConditionAlways extends EffectCondition {

	@Override
	public boolean isFulFilled(EntityPlayer player) {
		return true;
	}

	@Override
	public void readSettings(JsonObject json) {
		
	}

}
