package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonObject;

import net.minecraft.entity.player.EntityPlayer;

public class EffectConditionMoving extends EffectCondition {

	@Override
	public boolean isFulFilled(EntityPlayer player) {
		return player.posX != player.prevPosX || player.posY != player.prevPosY || player.posZ != player.prevPosZ;
	}

	@Override
	public void readSettings(JsonObject json) {
		
	}

}
