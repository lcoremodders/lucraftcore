package lucraft.mods.lucraftcore.superpowers.capabilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability.AbilityComparator;
import lucraft.mods.lucraftcore.superpowers.abilities.IAbilityContainer;
import lucraft.mods.lucraftcore.superpowers.effects.EffectTrail.EntityTrail;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import lucraft.mods.lucraftcore.superpowers.network.MessageChooseSuperpowerGui;
import lucraft.mods.lucraftcore.superpowers.network.MessageSyncSuperpower;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CapabilitySuperpower implements ISuperpowerCapability {

	@CapabilityInject(ISuperpowerCapability.class)
	public static final Capability<ISuperpowerCapability> SUPERPOWER_CAP = null;

	public EntityPlayer player;
	public Superpower superpower;
	public SuperpowerPlayerHandler handler;

	public SuitSet suitSet;
	public SuitSet prevSuitSet;
	public SuitSetAbilityHandler suitSetAbilityHandler;

	public NBTTagCompound superpowerData = new NBTTagCompound();
	public NBTTagCompound suitSetData = new NBTTagCompound();

	public boolean hasPlayedBefore = false;
	
	public LinkedList<Entity> trailEntities = new LinkedList<>();
	
	public CapabilitySuperpower(EntityPlayer player) {
		this.player = player;
	}

	@Override
	public void setSuperpower(Superpower superpower, boolean update) {
		if (this.superpower != superpower && handler != null) {
			for (Ability ab : handler.getAbilities()) {
				if (ab.isUnlocked())
					ab.lastTick();
			}
			
			handler.onRemove();
		}

		this.superpower = superpower;
		
		if (this.handler != null) {
			NBTTagCompound nbt = this.handler.writeToNBT(new NBTTagCompound());
			this.superpowerData.setTag(this.handler.superpower.getRegistryName().toString(), nbt);
		}

		if (update)
			this.syncToAll();
	}

	@Override
	public void setSuperpower(Superpower superpower) {
		this.setSuperpower(superpower, true);
	}

	@Override
	public Superpower getSuperpower() {
		return this.superpower;
	}

	@Override
	public SuperpowerPlayerHandler getSuperpowerPlayerHandler() {
		return this.handler;
	}

	@Override
	public SuitSetAbilityHandler getSuitSetAbilityHandler() {
		return this.suitSetAbilityHandler;
	}

	@Override
	public boolean hasPlayedBefore() {
		return hasPlayedBefore;
	}
	
	@Override
	public void setHasPlayedBefore(boolean played) {
		this.hasPlayedBefore = played;
	}
	
	@Override
	public void onUpdate(Phase phase) {
		if (handler != null)
			handler.update(phase);
		if (phase == Phase.START) {
			prevSuitSet = suitSet;
			suitSet = SuitSet.getSuitSet(player);
			if (suitSet != null)
				suitSet.onUpdate(player);
			if (suitSet != prevSuitSet) {

				if (suitSetAbilityHandler != null) {
					for (Ability ab : suitSetAbilityHandler.abilities)
						ab.lastTick();
				}

				suitSetAbilityHandler = null;

				if (prevSuitSet != null)
					prevSuitSet.onUnequip(player);

				if (suitSet != null) {
					suitSet.onEquip(player);
					suitSetAbilityHandler = new SuitSetAbilityHandler(suitSet, player);
					this.syncToAll();
				}
			}

			if (suitSetAbilityHandler != null)
				suitSetAbilityHandler.onUpdate();
		}
		
		if(!hasPlayedBefore() && LCConfig.superpowers.startSuperpowersEnabled && player.ticksExisted == 100) {
			if(player instanceof EntityPlayerMP) {
				LCPacketDispatcher.sendTo(new MessageChooseSuperpowerGui(LCConfig.superpowers.startSuperpowers), (EntityPlayerMP) player);
			}
		}
	}

	@Override
	public EntityPlayer getPlayer() {
		return this.player;
	}

	@Override
	public NBTTagCompound writeNBT() {
		NBTTagCompound nbt = new NBTTagCompound();

		if (this.superpower != null)
			nbt.setString("Superpower", this.superpower.getRegistryName().toString());

		if (this.handler != null) {
			NBTTagCompound tag = this.handler.writeToNBT(new NBTTagCompound());
			this.superpowerData.setTag(this.handler.superpower.getRegistryName().toString(), tag);
		}
		nbt.setTag("SuperpowerData", this.superpowerData);

		if (this.suitSetAbilityHandler != null) {
			NBTTagCompound suitData = this.suitSetAbilityHandler.writeNBT(new NBTTagCompound());
			nbt.setTag("SuitSetData", suitData);
		}
		
		nbt.setBoolean("HasPlayedBefore", hasPlayedBefore);

		return nbt;
	}

	@Override
	public void readNBT(NBTTagCompound nbt) {
		this.superpowerData = nbt.getCompoundTag("SuperpowerData");
		this.superpower = SuperpowerHandler.SUPERPOWER_REGISTRY.getValue(new ResourceLocation(nbt.getString("Superpower")));
		this.suitSetData = nbt.getCompoundTag("SuitSetData");
		this.hasPlayedBefore = nbt.getBoolean("HasPlayedBefore");
	}

	@Override
	public void loadSuperpowerHandler() {
		if (this.superpower != null) {
			this.handler = this.superpower.getNewSuperpowerHandler(this);
			this.handler.readFromNBT(this.superpowerData.getCompoundTag(this.handler.superpower.getRegistryName().toString()));
		} else {
			this.handler = null;
		}
	}

	@Override
	public void loadSuitSetHandler() {
		if (suitSet != null) {
			this.suitSetAbilityHandler = new SuitSetAbilityHandler(suitSet, player);
			this.suitSetAbilityHandler.readNBT(suitSetData);
		}
	}

	@Override
	public void syncToPlayer() {
		this.syncToPlayer(this.player);
	}

	@Override
	public void syncToPlayer(EntityPlayer receiver) {
		if (receiver instanceof EntityPlayerMP)
			LCPacketDispatcher.sendTo(new MessageSyncSuperpower(this.player), (EntityPlayerMP) receiver);
	}

	@Override
	public void syncToAll() {
		this.syncToPlayer();
		if (player.world instanceof WorldServer) {
			for (EntityPlayer players : ((WorldServer) player.world).getEntityTracker().getTrackingPlayers(player)) {
				if (players instanceof EntityPlayerMP) {
					LCPacketDispatcher.sendTo(new MessageSyncSuperpower(player), (EntityPlayerMP) players);
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public LinkedList<EntityTrail> getTrailEntities() {
		LinkedList<EntityTrail> list = new LinkedList<>();
		for(Entity en : this.trailEntities) {
			if(en instanceof EntityTrail) {
				list.add((EntityTrail) en);
			}
		}
		return list;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addTrailEntity(Entity entity) {
		this.trailEntities.add(entity);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void removeTrailEntity(Entity entity) {
		this.trailEntities.remove(entity);
	}
	
	// ----------------------------------------------------------------------------------------------------------------------------

	public static class CapabilitySuperpowerEventHandler {

		@SubscribeEvent
		public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> evt) {
			if (!(evt.getObject() instanceof EntityPlayer))
				return;

			evt.addCapability(new ResourceLocation(LucraftCore.MODID, "superpower_capability"), new CapabilitySuperpowerProvider(new CapabilitySuperpower((EntityPlayer) evt.getObject())));
		}

		@SubscribeEvent
		public void onPlayerStartTracking(PlayerEvent.StartTracking e) {
			if (e.getTarget().hasCapability(CapabilitySuperpower.SUPERPOWER_CAP, null)) {
				e.getTarget().getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null).syncToPlayer(e.getEntityPlayer());
			}
		}

		@SubscribeEvent
		public void onEntityJoinWorld(EntityJoinWorldEvent e) {
			if (e.getEntity() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntity();
				ISuperpowerCapability data = player.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null);
				data.loadSuperpowerHandler();
				SuperpowerHandler.syncToAll(player);
			}
		}

		@SubscribeEvent
		public void onPlayerClone(PlayerEvent.Clone e) {
			NBTTagCompound compound = new NBTTagCompound();
			compound = (NBTTagCompound) CapabilitySuperpower.SUPERPOWER_CAP.getStorage().writeNBT(CapabilitySuperpower.SUPERPOWER_CAP, e.getOriginal().getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null), null);
			CapabilitySuperpower.SUPERPOWER_CAP.getStorage().readNBT(CapabilitySuperpower.SUPERPOWER_CAP, e.getEntityPlayer().getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null), null, compound);
		}

		@SubscribeEvent
		public void onUpdate(PlayerTickEvent e) {
			e.player.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null).onUpdate(e.phase);
		}

		@SubscribeEvent
		public void onLivingAttackEvent(LivingAttackEvent e) {
			if (e.getEntityLiving() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntityLiving();

				for (Ability ab : Ability.getCurrentPlayerAbilities(player)) {
					if (ab.isUnlocked()) {
						ab.onAttacked(e);
					}
				}
			}
		}

		@SubscribeEvent
		public void onLivingHurtEvent(LivingHurtEvent e) {
			if (e.getEntityLiving() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntityLiving();

				for (Ability ab : Ability.getCurrentPlayerAbilities(player)) {
					if (ab.isUnlocked()) {
						ab.onPlayerHurt(e);
					}
				}
			}

			if (e.getSource() != null && e.getSource().getImmediateSource() != null && e.getSource().getImmediateSource() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getSource().getImmediateSource();

				for (Ability ab : Ability.getCurrentPlayerAbilities(player)) {
					if (ab.isUnlocked()) {
						ab.onHurt(e);
					}
				}
			}

		}

		@SubscribeEvent
		public void onAttackEntityEvent(AttackEntityEvent e) {
			if (e.getEntityLiving() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntityLiving();

				for (Ability ab : Ability.getCurrentPlayerAbilities(player)) {
					if (ab.isUnlocked()) {
						ab.onAttackEntity(e);
					}
				}
			}
		}

		@SubscribeEvent
		public void onBreakSpeed(BreakSpeed e) {
			if (e.getEntityLiving() instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) e.getEntityLiving();

				for (Ability ab : Ability.getCurrentPlayerAbilities(player)) {
					if (ab.isUnlocked()) {
						ab.onBreakSpeed(e);
					}
				}
			}
		}

	}

	public static class CapabilitySuperpowerStorage implements IStorage<ISuperpowerCapability> {

		@Override
		public NBTBase writeNBT(Capability<ISuperpowerCapability> capability, ISuperpowerCapability instance, EnumFacing side) {
			return instance.writeNBT();
		}

		@Override
		public void readNBT(Capability<ISuperpowerCapability> capability, ISuperpowerCapability instance, EnumFacing side, NBTBase nbt) {
			instance.readNBT((NBTTagCompound) nbt);
		}

	}

	public static class SuitSetAbilityHandler implements IAbilityContainer {

		public final SuitSet suitset;
		public final EntityPlayer player;

		private List<Ability> abilities;

		public SuitSetAbilityHandler(SuitSet suitset, EntityPlayer player) {
			this.suitset = suitset;
			this.player = player;
			this.abilities = suitset.getDefaultAbilities(player, new ArrayList<>());
			Collections.sort(this.abilities, new AbilityComparator());
		}

		@Override
		public List<Ability> getAbilities() {
			return abilities;
		}

		public void onUpdate() {
			for (Ability ab : getAbilities()) {
				ab.onUpdate();
			}
		}

		public NBTTagCompound writeNBT(NBTTagCompound nbt) {
			NBTTagList tagList = new NBTTagList();

			for (Ability ability : getAbilities()) {
				tagList.appendTag(ability.serializeNBT());
			}

			nbt.setTag("SuitAbilities", tagList);
			return nbt;
		}

		public void readNBT(NBTTagCompound compound) {
			if (this.abilities != null)
				this.abilities.clear();

			if (compound.getTagList("SuitAbilities", 10).tagCount() > 0) {
				List<Ability> list = suitset.getDefaultAbilities(player, new ArrayList<>());
				NBTTagList tagList = compound.getTagList("SuitAbilities", 10);

				for (int i = 0; i < tagList.tagCount(); i++) {
					NBTTagCompound nbt = tagList.getCompoundTagAt(i);
					Class<? extends Ability> clz = Ability.ABILITY_REGISTRY.getValue(new ResourceLocation(nbt.getString("Ability"))).getAbilityClass();

					for (Ability ab : list) {
						if (ab.getClass() == clz) {
							ab.deserializeNBT(nbt);
						}
					}
				}
				Collections.sort(list, new AbilityComparator());
				this.abilities = list;
			} else {
				List<Ability> list = suitset.getDefaultAbilities(player, new ArrayList<>());
				Collections.sort(list, new AbilityComparator());
				this.abilities = list;
			}
		}

		@Override
		public Ability getAbilityForKey(AbilityKeys key) {
			return this.suitset.getSuitAbilityForKey(key, getAbilities());
		}

	}

}
