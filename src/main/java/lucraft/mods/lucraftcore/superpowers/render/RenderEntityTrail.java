package lucraft.mods.lucraftcore.superpowers.render;

import lucraft.mods.lucraftcore.superpowers.effects.EffectTrail.EntityTrail;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderEntityTrail extends Render<EntityTrail> {

	public RenderEntityTrail(RenderManager renderManager) {
		super(renderManager);
	}

	@Override
	public void doRender(EntityTrail entity, double x, double y, double z, float entityYaw, float partialTicks) {

	}

	@Override
	protected ResourceLocation getEntityTexture(EntityTrail entity) {
		return null;
	}

}
