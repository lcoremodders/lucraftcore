package lucraft.mods.lucraftcore.superpowers.render;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.effects.*;
import lucraft.mods.lucraftcore.superpowers.effects.EffectTrail.EntityTrail;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SuperpowerRenderer implements LayerRenderer<EntityPlayer> {

	public RenderPlayer renderer;
	public static Minecraft mc = Minecraft.getMinecraft();
	private static boolean eventHandlerRegistered = false;
	public static final ResourceLocation WHITE_TEX = new ResourceLocation(LucraftCore.MODID, "textures/models/white.png");

	public SuperpowerRenderer(RenderPlayer renderer) {
		this.renderer = renderer;

		if (!eventHandlerRegistered) {
			MinecraftForge.EVENT_BUS.register(this);
			eventHandlerRegistered = true;
		}
	}

	@Override
	public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		if (player.getActivePotionEffect(MobEffects.INVISIBILITY) != null)
			return;

		Superpower superpower = SuperpowerHandler.getSuperpower(player);
		if (superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderPlayer(renderer, mc, player, superpower, SuperpowerHandler.getSuperpowerPlayerHandler(player), limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale);

		// Effect: Glow
		mc.renderEngine.bindTexture(WHITE_TEX);
		for (EffectGlow glow : EffectHandler.getEffectsByClass(player, EffectGlow.class)) {
			if (EffectHandler.canEffectBeDisplayed(glow, player)) {
				GlStateManager.pushMatrix();
				ModelPlayer model = new ModelPlayer(glow.size, PlayerHelper.hasSmallArms(player));
				model.isChild = false;
				model.setModelAttributes(renderer.getMainModel());
				LCRenderHelper.setLightmapTextureCoords(240, 240);
				GlStateManager.disableLighting();
				GlStateManager.enableBlend();
				GlStateManager.color((float) glow.color.x, (float) glow.color.y, (float) glow.color.z, 0.5F);
				model.render(player, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
				LCRenderHelper.restoreLightmapTextureCoords();
				GlStateManager.enableLighting();
				GlStateManager.disableBlend();
				GlStateManager.popMatrix();
			}
		}

		// Effect: Vibrating
		if (EffectVibrating.isVibrating(player) && !SuitSet.hasSuitSetOn(player)) {
			mc.renderEngine.bindTexture(renderer.getEntityTexture((AbstractClientPlayer) player));
			for (int i = 0; i < 10; i++) {
				GlStateManager.pushMatrix();
				Random rand = new Random();
				GlStateManager.translate((rand.nextFloat() - 0.5F) / 15, 0, (rand.nextFloat() - 0.5F) / 15);
				GlStateManager.color(1, 1, 1, 0.3F);
				this.renderer.getMainModel().render(player, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
				GlStateManager.popMatrix();
			}
		}

		// Effect: Flickering
		List<EffectFlickering> flickerings = EffectHandler.getEffectsByClass(player, EffectFlickering.class);
		for (int i = 0; i < flickerings.size(); i++) {
			EffectFlickering flicker = flickerings.get(i);
			LCRenderHelper.setupRenderLightning();
			GlStateManager.translate(0, -0.2F, 0);
			if (EffectHandler.canEffectBeDisplayed(flicker, player)) {
				AxisAlignedBB box = new AxisAlignedBB(-player.width / 2D, 0, -player.width / 2D, player.width / 2D, player.height, player.width / 2D);
				float thickness = 0.001F;
				LCRenderHelper.drawRandomLightningCoordsInAABB(thickness, flickerings.get(i).getColor(player), box, new Random(i + player.ticksExisted / 2));
			}
			LCRenderHelper.finishRenderLightning();
		}
	}

	@SubscribeEvent
	public void onSetRotationAngels(RenderModelEvent.SetRotationAngels e) {
		if (e.getEntity() instanceof EntityPlayer && e.model instanceof ModelPlayer) {
			EntityPlayer player = (EntityPlayer) e.getEntity();
			for (EffectSkinChange skinChange : EffectHandler.getEffectsByClass(player, EffectSkinChange.class)) {
				if (EffectHandler.canEffectBeDisplayed(skinChange, player)) {
					mc.renderEngine.bindTexture(skinChange.texture);
				}
			}
		}
	}

	@SubscribeEvent
	public void onRenderSpecificHand(RenderSpecificHandEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if (superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderSpecificHandEvent(e);
	}

	@SubscribeEvent
	public void onRenderHand(RenderHandEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if (superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderHandEvent(e);
	}

	@SubscribeEvent
	public void onRenderHUD(RenderGameOverlayEvent e) {
		Superpower superpower = SuperpowerHandler.getSuperpower(mc.player);
		if (superpower != null && superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().onRenderGameOverlay(e);
	}

	@SubscribeEvent
	public void onRenderPlayerPost(RenderPlayerEvent.Post e) {
		List<EffectTrail> trailEffects = new ArrayList<>();
		for (EntityTrail trails : SuperpowerHandler.getSuperpowerData(e.getEntityPlayer()).getTrailEntities()) {
			for (EffectTrail effects : trails.effects) {
				if (!trailEffects.contains(effects)) {
					trailEffects.add(effects);
				}
			}
		}

		for (EffectTrail trails : trailEffects) {
			trails.type.getTrailRenderer().renderTrail(e.getEntityPlayer(), trails, SuperpowerHandler.getSuperpowerData(e.getEntityPlayer()).getTrailEntities());
		}
	}

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent e) {
		if (e.getEntityLiving() instanceof EntityPlayer) {
			List<EffectTrail> trailEffects = new ArrayList<>();
			for (EffectTrail trails : EffectHandler.getEffectsByClass((EntityPlayer) e.getEntityLiving(), EffectTrail.class)) {
				if (EffectHandler.canEffectBeDisplayed(trails, (EntityPlayer) e.getEntityLiving())) {
					trailEffects.add(trails);
				}
			}

			if (trailEffects.size() > 0) {

				ISuperpowerCapability cap = SuperpowerHandler.getSuperpowerData((EntityPlayer) e.getEntityLiving());
				LinkedList<EntityTrail> list = cap.getTrailEntities();
				
				if (list.size() == 0) {
					EntityTrail trail = new EntityTrail(e.getEntityLiving().getEntityWorld(), (EntityPlayer) e.getEntityLiving(), trailEffects.toArray(new EffectTrail[trailEffects.size()]));
					cap.addTrailEntity(trail);
					e.getEntityLiving().getEntityWorld().spawnEntity(trail);
				} else if (e.getEntityLiving().getDistance(list.getLast()) >= e.getEntityLiving().width * 1.1F) {
					EntityTrail trail = new EntityTrail(e.getEntityLiving().getEntityWorld(), (EntityPlayer) e.getEntityLiving(), trailEffects.toArray(new EffectTrail[trailEffects.size()]));
					cap.addTrailEntity(trail);
					e.getEntityLiving().getEntityWorld().spawnEntity(trail);
				}
			}
		}
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	public static interface ISuperpowerRenderer {

		@SideOnly(Side.CLIENT)
		public void onRenderPlayer(RenderLivingBase<?> renderer, Minecraft mc, EntityPlayer player, Superpower superpower, SuperpowerPlayerHandler handler, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float mcScale);

		public default void applyColor() {
			GlStateManager.color(1, 1, 1);
		}

		public default void onRenderHandEvent(RenderHandEvent e) {
		}

		public default void onRenderSpecificHandEvent(RenderSpecificHandEvent e) {
		}

		public default void onRenderGameOverlay(RenderGameOverlayEvent e) {
		}

		/**
		 * Gets called when RenderPlayerAPI is installed
		 * 
		 * @param side
		 */
		public default void onRenderFirstPersonArmRPAPI(EnumHandSide side) {
		}

	}

}
