package lucraft.mods.lucraftcore.superpowers.commands;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;

public class CommandSuperpower extends CommandBase {

	@Override
	public String getName() {
		return "superpower";
	}
	
	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.superpower.usage";
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if(args.length == 0 || args.length > 2) {
			throw new WrongUsageException("commands.superpower.usage", new Object[0]);
		} else {
			EntityPlayer player = getPlayer(server, sender, args[0]);
			
			if(args.length == 1) {
				Superpower power = SuperpowerHandler.getSuperpower(player);
				if(power == null)
					sender.sendMessage(new TextComponentTranslation("lucraftcore.info.superpower.playernopower", new Object[] { player.getName() }));
				else
					sender.sendMessage(new TextComponentTranslation("lucraftcore.info.superpower.playerspower", new Object[] { player.getName(), power.getDisplayName() }));
			} else {
				if(args[1].equalsIgnoreCase("remove")) {
					SuperpowerHandler.removeSuperpower(player);
					
					if (player != sender) {
						notifyCommandListener(sender, this, 1, "commands.superpower.remove.success.other", new Object[] { player.getName() });
					} else {
						sender.sendMessage(new TextComponentTranslation("commands.superpower.remove.success.self", new Object[] { }));
					}
					
				} else {
					Superpower power = SuperpowerHandler.SUPERPOWER_REGISTRY.getValue(new ResourceLocation(args[1]));
					
					if(power == null)
						sender.sendMessage(new TextComponentTranslation("lucraftcore.info.superpower.powernotfound", new Object[0]));
					else {
						SuperpowerHandler.setSuperpower(player, power);
						
						if (player != sender) {
							notifyCommandListener(sender, this, 1, "commands.superpower.success.other", new Object[] { player.getName(), power.getDisplayName() });
						} else {
							sender.sendMessage(new TextComponentTranslation("commands.superpower.success.self", new Object[] { power.getDisplayName() }));
						}
					}
				}
			}
		}
	}

	public boolean isUsernameIndex(String[] args, int index) {
		return index == 0;
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
		ArrayList<String> list = new ArrayList<String>();
		
		for(ResourceLocation power : SuperpowerHandler.SUPERPOWER_REGISTRY.getKeys())
			list.add(power.toString());
		
		list.add("remove");
		
		return args.length == 1 ? (getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames())) : getListOfStringsMatchingLastWord(args, list);
	}
}
