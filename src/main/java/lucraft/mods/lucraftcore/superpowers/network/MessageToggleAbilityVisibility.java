package lucraft.mods.lucraftcore.superpowers.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.List;

public class MessageToggleAbilityVisibility implements IMessage {

	public int ability;

	public MessageToggleAbilityVisibility() {
	}

	public MessageToggleAbilityVisibility(int ability) {
		this.ability = ability;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		ability = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(ability);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageToggleAbilityVisibility> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageToggleAbilityVisibility message, MessageContext ctx) {
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					List<Ability> abilities = SuperpowerHandler.getSuperpowerPlayerHandler(player).getAbilities();

					if (abilities != null && abilities.size() > message.ability) {
						Ability ability = abilities.get(message.ability);
						ability.setHidden(!ability.isHidden());
						SuperpowerHandler.syncToPlayer(player);
					}
				}
			});
			return null;
		}

	}

}