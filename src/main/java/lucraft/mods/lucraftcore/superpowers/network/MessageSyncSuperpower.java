package lucraft.mods.lucraftcore.superpowers.network;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSyncSuperpower implements IMessage {

	public UUID playerUUID;
	public NBTTagCompound nbt;
	
	public MessageSyncSuperpower() {
	}
	
	public MessageSyncSuperpower(EntityPlayer player) {
		this.playerUUID = player.getPersistentID();
		
		nbt = (NBTTagCompound) CapabilitySuperpower.SUPERPOWER_CAP.getStorage().writeNBT(CapabilitySuperpower.SUPERPOWER_CAP, player.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null), null);
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		this.playerUUID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.nbt = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.playerUUID.toString());
		ByteBufUtils.writeTag(buf, this.nbt);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSyncSuperpower> {

		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSyncSuperpower message, MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					if(message != null && ctx != null) {
						EntityPlayer en = LucraftCore.proxy.getPlayerEntity(ctx).world.getPlayerEntityByUUID(message.playerUUID);
						
						if(en != null) {
							CapabilitySuperpower.SUPERPOWER_CAP.getStorage().readNBT(CapabilitySuperpower.SUPERPOWER_CAP, en.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null), null, message.nbt);
							en.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null).loadSuperpowerHandler();
							en.getCapability(CapabilitySuperpower.SUPERPOWER_CAP, null).loadSuitSetHandler();
						}
					}
				}
				
			});
			
			return null;
		}
		
	}
	
}
