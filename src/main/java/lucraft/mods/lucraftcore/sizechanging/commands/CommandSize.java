package lucraft.mods.lucraftcore.sizechanging.commands;

import lucraft.mods.lucraftcore.sizechanging.capabilities.CapabilitySizeChanging;
import net.minecraft.command.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.List;

public class CommandSize extends CommandBase {

	@Override
	public String getName() {
		return "size";
	}

	public int getRequiredPermissionLevel() {
		return 2;
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.size.usage";
	}

	// /size [Size] OR /size <Entity> [Size]
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if(args.length > 2)
            throw new WrongUsageException("commands.size.usage", new Object[0]);
        else {

            if(args.length == 0) {
                if(sender instanceof EntityPlayer) {
                    EntityPlayer player = (EntityPlayer)sender;
                    float size = player.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getSize();
                    player.sendMessage(new TextComponentTranslation("commands.size.yoursize", size));
                } else {
                    sender.sendMessage(new TextComponentTranslation("commands.size.notaplayer"));
                }
            } else if(args.length == 1) {
                try {
                    Entity entity = getEntity(server, sender, args[0]);
                    if(!(entity instanceof EntityLivingBase))
                        throw new WrongUsageException("commands.size.wrongentity");
                    float size = entity.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getSize();
                    sender.sendMessage(new TextComponentTranslation("commands.size.entitysize", entity.getDisplayName(), size));
                } catch (EntityNotFoundException e) {
                    if(sender instanceof EntityPlayer) {
                        EntityPlayer player = (EntityPlayer)sender;
                        float size = (float)parseDouble(args[0], CapabilitySizeChanging.MIN_SIZE, CapabilitySizeChanging.MAX_SIZE);
                        player.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).setSize(size);
                    } else {
                        sender.sendMessage(new TextComponentTranslation("commands.size.notaplayer"));
                    }
                }
            } else {
                List<Entity> entities = getEntityList(server, sender, args[0]);
                float size = (float)parseDouble(args[1], CapabilitySizeChanging.MIN_SIZE, CapabilitySizeChanging.MAX_SIZE);

                for(Entity entity : entities) {
                    if(entity instanceof EntityLivingBase) {
                        entity.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).setSize(size);
                        sender.sendMessage(new TextComponentTranslation("commands.size.entitysizechanged", entity.getDisplayName(), size));
                    }
                }
            }

        }
	}
}
