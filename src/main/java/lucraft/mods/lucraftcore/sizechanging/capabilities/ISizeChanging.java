package lucraft.mods.lucraftcore.sizechanging.capabilities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public interface ISizeChanging {

    public void tick();

    public float getSize();
    public float getRenderSize(float partialTick);
    public void setSize(float size);

    public void setOriginalSize(float width, float height);
    public float getOriginalWidth();
    public float getOriginalHeight();

    public NBTTagCompound writeNBT();
    public void readNBT(NBTTagCompound nbt);

    void syncToPlayer(EntityPlayer receiver);
    void syncToAll();

}