package lucraft.mods.lucraftcore.sizechanging.render;

import lucraft.mods.lucraftcore.sizechanging.capabilities.CapabilitySizeChanging;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SizeChangingRenderer {

	@SubscribeEvent
	public void renderEntityPre(RenderLivingEvent.Pre e) {
		if (!e.getEntity().hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return;
		float scale = e.getEntity().getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getRenderSize(e.getPartialRenderTick());

		GlStateManager.pushMatrix();

		GlStateManager.scale(scale, scale, scale);
		GlStateManager.translate((e.getX() / scale) - e.getX(), (e.getY() / scale) - e.getY(), (e.getZ() / scale) - e.getZ());
		if (e.getEntity().isSneaking()) {
			GlStateManager.translate(0, 0.125F / scale, 0);
			GlStateManager.translate(0, -0.125F, 0);
		}
	}

	@SubscribeEvent
	public void renderEntityPost(RenderLivingEvent.Post e) {
		if (!e.getEntity().hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return;
		GlStateManager.popMatrix();
	}

	@SubscribeEvent
	public void renderEntityNamePre(RenderLivingEvent.Specials.Pre e) {
		if (!e.getEntity().hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return;
		float scale = e.getEntity().getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getRenderSize(e.getPartialRenderTick());

		GlStateManager.pushMatrix();

		boolean flag = e.getEntity().isSneaking();
		float vanillaOffset = e.getEntity().height + 0.5F - (flag ? 0.25F : 0.0F);

		GlStateManager.translate(0, -vanillaOffset, 0);

		float adjustedOffset = (e.getEntity().height / scale) + (0.5F) - (flag ? 0.25F : 0F);

		GlStateManager.translate(0, adjustedOffset, 0);
	}

	@SubscribeEvent
	public void renderEntityNamePost(RenderLivingEvent.Specials.Post e) {
		if (!e.getEntity().hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return;

		GlStateManager.popMatrix();
	}

	@SubscribeEvent
	public void setupCamera(EntityViewRenderEvent.CameraSetup e) {
		if (!e.getEntity().hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return;
		float scale = e.getEntity().getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getRenderSize((float)e.getRenderPartialTicks());

		if (!(e.getEntity() instanceof EntityLivingBase && ((EntityLivingBase) e.getEntity()).isPlayerSleeping()) && Minecraft.getMinecraft().gameSettings.thirdPersonView == 0) {
			GlStateManager.translate(0, 0, -0.05F);
			GlStateManager.translate(0, 0, (scale * 0.05F));
		}
	}

}
