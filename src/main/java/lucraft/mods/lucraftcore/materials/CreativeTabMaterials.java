package lucraft.mods.lucraftcore.materials;

import lucraft.mods.lucraftcore.materials.Material.MaterialComponent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CreativeTabMaterials extends CreativeTabs {

	public CreativeTabMaterials(String label) {
		super(label);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ItemStack getTabIconItem() {
		return Material.IRIDIUM.getItemStack(MaterialComponent.PLATE);
	}
	@Override
	public boolean hasSearchBar() {
		return true;
	}
	
	public int getSearchbarWidth() {
		return 40;
	}
	
	@Override
	public String getBackgroundImageName() {
		return "lucraftcoremetals.png";
	}
	
	@Override
	public void displayAllRelevantItems(NonNullList<ItemStack> list) {
		for(Material m : Material.getMaterials()) {
			NonNullList<ItemStack> l = NonNullList.create();
			for(MaterialComponent c : MaterialComponent.values()) {
				ItemStack stack = m.getItemStack(c);
				
				if(!stack.isEmpty())
					l.add(stack);
			}
			while(l.size() < 9)
				l.add(ItemStack.EMPTY);
			list.addAll(l);
		}
	}

}
