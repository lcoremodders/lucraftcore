package lucraft.mods.lucraftcore.utilities;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.util.gui.LCGuiHandler;
import lucraft.mods.lucraftcore.utilities.blocks.TileEntityConstructionTable;
import lucraft.mods.lucraftcore.utilities.blocks.UtilitiesBlocks;
import lucraft.mods.lucraftcore.utilities.gui.GuiHandlerEntryConstructionTable;
import lucraft.mods.lucraftcore.utilities.items.ItemInjection;
import lucraft.mods.lucraftcore.utilities.items.UtilitiesItems;
import lucraft.mods.lucraftcore.utilities.network.MessageClearInstructionRecipes;
import lucraft.mods.lucraftcore.utilities.network.MessageSetSelectedRecipe;
import lucraft.mods.lucraftcore.utilities.network.MessageSyncInstructionRecipe;
import lucraft.mods.lucraftcore.utilities.recipes.AddonPackInstructionRecipeReader;
import lucraft.mods.lucraftcore.utilities.render.TESRConstructionTable;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModuleUtilities extends Module {

	public static final ModuleUtilities INSTANCE = new ModuleUtilities();
	
	public UtilitiesItems ITEMS = new UtilitiesItems();
	public UtilitiesBlocks BLOCKS = new UtilitiesBlocks();
	
	@Override
	public void preInit(FMLPreInitializationEvent event) {
		// EventHandler Registering
		MinecraftForge.EVENT_BUS.register(ITEMS);
		MinecraftForge.EVENT_BUS.register(BLOCKS);
		
		// Gui Handler
		LCGuiHandler.registerGuiHandlerEntry(GuiHandlerEntryConstructionTable.ID, new GuiHandlerEntryConstructionTable());
	}

	@Override
	public void init(FMLInitializationEvent event) {
		// Packet Registering
		LCPacketDispatcher.registerMessage(MessageSetSelectedRecipe.Handler.class, MessageSetSelectedRecipe.class, Side.SERVER, 20);
		LCPacketDispatcher.registerMessage(MessageClearInstructionRecipes.Handler.class, MessageClearInstructionRecipes.class, Side.CLIENT, 21);
		LCPacketDispatcher.registerMessage(MessageSyncInstructionRecipe.Handler.class, MessageSyncInstructionRecipe.class, Side.CLIENT, 22);
		
		// Instruction Recipes from Addon Packs
		AddonPackInstructionRecipeReader.loadRecipes();
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {
		LucraftCore.CREATIVE_TAB_ICON = new ItemStack(ITEMS.HAMMER);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void initClient(FMLInitializationEvent event) {
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new ItemInjection.InjectionItemColor(), UtilitiesItems.INJECTION);
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityConstructionTable.class, new TESRConstructionTable());
	}
	
	@Override
	public String getName() {
		return "Utilities";
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}

}
