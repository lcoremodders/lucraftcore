package lucraft.mods.lucraftcore.karma.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.karma.capabilities.CapabilityKarma;
import lucraft.mods.lucraftcore.karma.capabilities.IKarmaCapability;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageToggleKnockOut implements IMessage {

	public MessageToggleKnockOut() {
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {

	}

	@Override
	public void toBytes(ByteBuf buf) {

	}
	
	public static class Handler extends AbstractServerMessageHandler<MessageToggleKnockOut> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageToggleKnockOut message, MessageContext ctx) {
			
			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(new Runnable() {

				@Override
				public void run() {
					IKarmaCapability cap = player.getCapability(CapabilityKarma.KARMA_CAP, null);
					cap.setKnockOutModeEnabled(!cap.isKnockOutModeEnabled());
				}
				
			});
			
			return null;
		}
		
	}

}
