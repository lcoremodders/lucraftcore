package lucraft.mods.lucraftcore.karma.keys;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.karma.capabilities.CapabilityKarma;
import lucraft.mods.lucraftcore.karma.capabilities.IKarmaCapability;
import lucraft.mods.lucraftcore.karma.network.MessageToggleKnockOut;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class KarmaKeyBindings {

	public static final KeyBinding KNOCK_OUT_MODE = new KeyBinding("lucraftcore.keybinding.keyKnockOut", KeyConflictContext.IN_GAME, Keyboard.KEY_COMMA, LucraftCore.NAME);
	
	private static int iconTimer = 0;
	private static final ResourceLocation knockOutOnTexture = new ResourceLocation(LucraftCore.MODID, "textures/gui/knockout_off.png");
	private static final ResourceLocation knockOutOffTexture = new ResourceLocation(LucraftCore.MODID, "textures/gui/knockout_on.png");
	
	public KarmaKeyBindings() {
		ClientRegistry.registerKeyBinding(KNOCK_OUT_MODE);
	}
	
	@SubscribeEvent
	public void onKey(KeyInputEvent evt) {
		if(KNOCK_OUT_MODE.isPressed()) {
			LCPacketDispatcher.sendToServer(new MessageToggleKnockOut());
			IKarmaCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityKarma.KARMA_CAP, null);
			cap.setKnockOutModeEnabled(!cap.isKnockOutModeEnabled());
			iconTimer = 20;
		}
	}
	
	@SubscribeEvent
	public void onClientTick(ClientTickEvent event) {
		if(event.phase == Phase.END && iconTimer > 0)
			iconTimer--;
	}
	
	@SubscribeEvent
	public void onRenderOverlay(RenderGameOverlayEvent.Post event) {
		if(event.getType() != ElementType.ALL || iconTimer == 0)
			return;
		
		Minecraft mc = Minecraft.getMinecraft();
		ScaledResolution res = event.getResolution();
		int x = res.getScaledWidth() / 2 - 16;
		int y = res.getScaledHeight() - 100;
		boolean knockOut = mc.player.getCapability(CapabilityKarma.KARMA_CAP, null).isKnockOutModeEnabled();
		float alpha = (iconTimer + event.getPartialTicks()) / 10F;

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.color(1F, 1F, 1F, alpha);
		mc.renderEngine.bindTexture(knockOut ? knockOutOffTexture : knockOutOnTexture);
		Gui.drawModalRectWithCustomSizedTexture(x, y, 0, 0, 32, 32, 32, 32);
		GlStateManager.popMatrix();
	}
	
}
