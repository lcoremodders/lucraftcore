package lucraft.mods.lucraftcore.test;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.util.vector.Vector3f;

import lucraft.mods.lucraftcore.util.render.IItemRenderer;
import lucraft.mods.lucraftcore.util.render.LCModelState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelZombie;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.block.model.ItemTransformVec3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.model.IModelState;
import net.minecraftforge.common.model.TRSRTransformation;

public class TestItemRenderer implements IItemRenderer {

	@Override
	public boolean isAmbientOcclusion() {
		return false;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public void renderItem(ItemStack stack, TransformType transformType) {
		ModelZombie model = new ModelZombie();
		ResourceLocation loc = new ResourceLocation("textures/entity/zombie/zombie.png");
		Minecraft.getMinecraft().renderEngine.bindTexture(loc);
		model.isChild = false;
		GlStateManager.rotate(180, 1, 0, 0);
		model.render(Minecraft.getMinecraft().player, 0, 0, 0, 0, 0, 0.0625F);
	}

	@Override
	public IModelState getTransforms() {
		Map map = new HashMap<ItemCameraTransforms.TransformType, TRSRTransformation>();
		map.put(TransformType.GUI, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(10F, -30F, 0), new Vector3f(0.5F, 0.73F, 0), new Vector3f(0.45F, 0.45F, 0.45F))));
		map.put(TransformType.GROUND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(-90F, 0F, 0F), new Vector3f(0.5F, 0.3F, 0.1F), new Vector3f(1F, 1F, 1F))));
		map.put(TransformType.FIRST_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.8F, 0.9F, 0F), new Vector3f(1F, 1F, 1F))));
		map.put(TransformType.FIRST_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, -50F, 0F), new Vector3f(0.2F, 0.9F, -0.8F), new Vector3f(1F, 1F, 1F))));
		map.put(TransformType.THIRD_PERSON_RIGHT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
		map.put(TransformType.THIRD_PERSON_LEFT_HAND, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(1.5F, 0.7F, 0.4F), new Vector3f(1F, 1F, 1F))));
		map.put(TransformType.FIXED, new TRSRTransformation(new ItemTransformVec3f(new Vector3f(0F, 180F, 0F), new Vector3f(0.5F, 0.75F, 0.5F), new Vector3f(0.5F, 0.5F, 0.5F))));
		return new LCModelState(map);
	}

}
