package lucraft.mods.lucraftcore.test;

import java.util.UUID;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityEnergyBlast;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityFallResistance;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityFirePunch;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityFlight;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHealth;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityJumpBoost;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityPotionPunch;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilitySlowfall;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityTeleport;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityWaterBreathing;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SuperpowerTest extends Superpower {

	public SuperpowerTest(String name) {
		super(name);
	}

	protected java.util.List<lucraft.mods.lucraftcore.superpowers.abilities.Ability> addDefaultAbilities(net.minecraft.entity.player.EntityPlayer player, java.util.List<lucraft.mods.lucraftcore.superpowers.abilities.Ability> list) {
		list.add(new AbilityJumpBoost(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 5, 0).setUnlocked(true));
		list.add(new AbilityHealth(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 10, 0).setUnlocked(true));
		list.add(new AbilityFallResistance(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 2, 0).setUnlocked(true));
		list.add(new AbilityTeleport(player, 30, 5*20).setUnlocked(true));
		list.add(new AbilityFirePunch(player, 30, 5*20).setUnlocked(true));
		list.add(new AbilityPotionPunch(player, MobEffects.LEVITATION, 1, 100, 5*20).setUnlocked(true));
		list.add(new AbilitySlowfall(player).setUnlocked(true));
		list.add(new AbilityFlight(player).setUnlocked(true));
		list.add(new AbilityWaterBreathing(player).setUnlocked(true));
		list.add(new AbilityEnergyBlast(player, 5*20, 5F, new Vec3d(1, 0, 0)).setUnlocked(true));
		return list;
	}
	
	@SideOnly(Side.CLIENT)
	public void renderIcon(net.minecraft.client.Minecraft mc, Gui gui, int x, int y) {
		GlStateManager.color(1, 1, 1);
		mc.renderEngine.bindTexture(TestMod.SUPERPOWER_ICON_TEX);
		gui.drawTexturedModalRect(x, y, 128, 0, 32, 32);
	}
	
	public boolean canLevelUp() {
		return true;
	}
	
	public int getMaxLevel() {
		return 10;
	}
	
	@SideOnly(Side.CLIENT)
	public boolean canCustomize() {
		return true;
	}
	
	@SideOnly(Side.CLIENT)
	public lucraft.mods.lucraftcore.superpowers.gui.GuiCustomizer getCustomizerGui(EntityPlayer player) {
		return new GuiCustomizerTest();
	}
	
	public net.minecraft.nbt.NBTTagCompound getDefaultStyleTag() {
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setFloat("Style", 0.5F);
		return nbt;
	}
	
	@SideOnly(Side.CLIENT)
	public int getCapsuleColor() {
		return 15073989;
	}
	
	@Override
	public SuperpowerPlayerHandler getNewSuperpowerHandler(ISuperpowerCapability cap) {
		return new SuperpowerPlayerHandler(cap, TestMod.TEST) {
			
			public int test;
			
			public void onUpdate(Phase phase) {
				if(getPlayer() == null) {
					return;
				}
				
				if(phase == Phase.END && getPlayer().ticksExisted % 40 == 0) {
//					test++;
//					getPlayer().sendMessage(new TextComponentString(cap.getCapabilityOwner().world.isRemote + ": " + test));
//					getPlayer().sendMessage(new TextComponentString("" + getAbilities().toString()));
//					for(Ability ab : getAbilities())
//						ab.setUnlocked(true);
//					this.addXP(800);
				}
			};
			
			public lucraft.mods.lucraftcore.superpowers.abilities.Ability getAbilityForKey(lucraft.mods.lucraftcore.superpowers.keys.AbilityKeys key) {
				if(key == AbilityKeys.SUPERPOWER_1)
					return Ability.getAbilityFromClass(getAbilities(), AbilityFirePunch.class);
				if(key == AbilityKeys.SUPERPOWER_2)
					return Ability.getAbilityFromClass(getAbilities(), AbilityTeleport.class);
				if(key == AbilityKeys.SUPERPOWER_3)
					return Ability.getAbilityFromClass(getAbilities(), AbilityFlight.class);
				if(key == AbilityKeys.SUPERPOWER_4)
					return Ability.getAbilityFromClass(getAbilities(), AbilitySlowfall.class);
				if(key == AbilityKeys.SUPERPOWER_5)
					return Ability.getAbilityFromClass(getAbilities(), AbilityEnergyBlast.class);
				return null;
			};
			
			public net.minecraft.nbt.NBTTagCompound writeToNBT(net.minecraft.nbt.NBTTagCompound compound) {
				super.writeToNBT(compound);
				compound.setInteger("TestInt", this.test);
				return compound;
			};
			
			public void readFromNBT(net.minecraft.nbt.NBTTagCompound compound) {
				super.readFromNBT(compound);
				this.test = compound.getInteger("TestInt");
			};
			
		};
	}
	
}